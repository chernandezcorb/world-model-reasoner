(define (domain poc)

    (:requirements :strips :typing :equality :quantified-preconditions :conditional-effects :fluents)

    (:types
        row - object
    )

    (:predicates
    	(front-occupied ?r - row)
        (front-blocked ?r - row)
    )

    (:functions
        (total-action-cost)
    	(max-objects-per-row)
    	(row-number-of-objects ?r - row)
    )

    (:action PUSH-BACK
        :parameters (?r - row)
        :precondition (and
        	(front-occupied ?r)  ; r has an object at the front
            (not (front-blocked ?r))  ; the front object is not blocked
			(< (row-number-of-objects ?r) (max-objects-per-row))  ; row r is not full
            (forall (?t - row)  ; all rows must at least have their front spot occupied
                (front-occupied ?t)
            )
        )
        :effect (and
            (increase (total-action-cost) 1)  ; add 1 to the total cost
        	(not (front-occupied ?r))  ; r does not have a object at the front anymore
        )
    )

    (:action PLACE
        :parameters (?r - row)
        :precondition (and
            (not(front-occupied ?r))  ; ; the front of row r is empty
        )
        :effect (and
            (increase (total-action-cost) 1)  ; add 1 to the total cost
        	(increase (row-number-of-objects ?r) 1)
        	(front-occupied ?r)
        )
    )

)
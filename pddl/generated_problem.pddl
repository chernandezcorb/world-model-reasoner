(define (problem stockashelf) 

 (:domain poc) 

 (:objects 

 r0 r1  - row 
 ) 

 (:init 
 (= (total-action-cost) 0) 
 (= (max-objects-per-row) 5) 
 (= (row-number-of-objects r0) 5) 
 (front-occupied r0) 
 (front-blocked r0) 
 (= (row-number-of-objects r1) 3) 
 (front-occupied r1) 
 ) 

 (:goal 
 (and 
 (forall (?r - row) 
 (or 
 (= (row-number-of-objects ?r) (max-objects-per-row)) 
 (and 
 (front-occupied ?r) 
 (front-blocked ?r) 
 ) 
 ) 
 ) 
 ) 
 ) 

 (:metric minimize (total-action-cost)) 
 )
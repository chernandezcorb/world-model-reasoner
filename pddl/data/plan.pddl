
ff: parsing domain file
domain 'POC' defined
 ... done.
ff: parsing problem file
problem 'STOCKASHELF' defined
 ... done.


translating negated cond for predicate FRONT-OCCUPIED
translating negated cond for predicate FRONT-BLOCKED
warning: numeric precondition. turning cost-minimizing relaxed plans OFF.

ff: search configuration is Enforced Hill-Climbing, then A*epsilon with weight 5.
Metric is ((1.00*[RF2](TOTAL-ACTION-COST)) - () + 0.00)
COST MINIMIZATION DONE (WITHOUT cost-minimizing relaxed plans).

Cueing down from goal distance:    4 into depth [1]
                                   3            [1][2]
                                   2            [1]
                                   1            [1]
                                   0            

ff: found legal plan as follows
step    0: PUSH-BACK R1
        1: PLACE R1
        2: PUSH-BACK R1
        3: PLACE R1
        4: REACH-GOAL
plan cost: 4.000000

time spent:    0.00 seconds instantiating 3 easy, 0 hard action templates
               0.00 seconds reachability analysis, yielding 3 facts and 4 actions
               0.00 seconds creating final representation with 3 relevant facts, 5 relevant fluents
               0.00 seconds computing LNF
               0.00 seconds building connectivity graph
               0.00 seconds searching, evaluating 6 states, to a max depth of 2
               0.00 seconds total time


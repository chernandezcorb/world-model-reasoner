(define (problem stockashelf)

    (:domain poc)

    (:objects
        r1 r2 r3 - row
    )

    (:init
        (= (total-action-cost) 0)
    	(= (max-objects-per-row) 3)
    	(= (row-number-of-objects r1) 0)  ; start with an empty shelf
        (= (row-number-of-objects r2) 0)  ; start with an empty shelf
        (= (row-number-of-objects r3) 0)  ; start with an empty shelf
        (front-blocked r1)
    )

    (:goal
        (forall (?r - row)
            (= (row-number-of-objects ?r) (max-objects-per-row))
        )
    )

    (:metric minimize (total-action-cost))
)

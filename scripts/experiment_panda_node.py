#!/usr/bin/env python

import rospy
import actionlib

from std_msgs.msg import *
from world_model_reasoner.msg import *

def stocking_client(shelf_name):
    result = StockShelfResult()
    result.success = False  # assume failure
    client = actionlib.SimpleActionClient("StockShelf", StockShelfAction)
    client.wait_for_server()
    goal = StockShelfGoal(shelf_name)
    client.send_goal(goal)
    # During action execution:
    client.wait_for_result()
    # When finished or out of time:
    if client.get_result() is not None:
        result = client.get_result()
    return result

def main_function():
    rospy.init_node("experiment_node")

    stock_resp = stocking_client("milkShelf")  # stock the shelf

if __name__ == "__main__":
    try:
        main_function()
    except rospy.ROSInterruptException:
        pass
#!/usr/bin/env python

import rospy
import pygame
import os
import numpy as np

import roslib; roslib.load_manifest("rosprolog")
from rosprolog_client import PrologException, Prolog

class visualisation:

        update_frequency = 1  # update the visualisation every second

        min_window_height = 300
        min_window_width = 200

        max_window_height = 1080
        max_window_width = 1530

        font_size = 40  # some dimensions in pixels
        base_object_dim = 120.0
        top_bar_height = 40
        bottom_bar_height = 20
        object_border_width = 5

        background_colour = (200, 200, 200)  # some colours of the visualisation
        bar_colour = (255, 255, 255)
        text_colour = (0, 0, 0)
        good_text_colour = (0, 150, 0)
        bad_text_colour = (150, 0, 0)
        seen_object_colour = (0, 200, 0)
        infered_object_colour = (0, 0, 200)
        action_object_colour = (200, 200, 0)
        unknown_detection_object_colour = (150, 0, 0)
        manipulable_object_border_colour = (0, 0, 0)
        immovable_object_border_colour = (255, 0, 0)


        def __init__(self, shelf_width_o, shelf_depth_o, object_width, object_depth, inter_row_distance):
                pygame.init()  # initialise the environment
                self.clock = pygame.time.Clock()  # start a clock
                self.object_width_dim = object_width  # save the physical dimensions of the object
                self.object_depth_dim = object_depth

                min_dim = min(object_width, object_depth)  # get the smallest dimension
                self.vis_r = visualisation.base_object_dim / min_dim  # calculate the ratio between real world distances and pixels (pixels/meter)
                self.object_width = int(object_width * self.vis_r)  # resize the object
                self.object_depth = int(object_depth * self.vis_r)
                self.inter_row_distance = inter_row_distance * self.vis_r  # distance in meters between the rows

                self.shelf_depth_objects = shelf_depth_o  # number of objects behind each other on the shelf
                self.shelf_width_objects = shelf_width_o  # number of objects beside each other on the shelf
                self.shelf_width = shelf_width_o * (self.object_width + self.inter_row_distance)
                self.shelf_height = shelf_depth_o * self.object_depth
                self.window_width = self.shelf_width
                self.window_height = self.shelf_height + visualisation.top_bar_height + visualisation.bottom_bar_height

                ratio = 1
                if (self.window_width > visualisation.max_window_width) or (self.window_height > visualisation.max_window_height):  # if the window is too big
                        ratio = max(float(self.window_width) / float(visualisation.max_window_width), float(self.window_height) / float(visualisation.max_window_height))  # calculate how much smaller the window must be
                elif (self.window_width < visualisation.min_window_width) or (self.window_height < visualisation.min_window_height):  # if the window is too small
                        ratio = min(float(self.window_width) / float(visualisation.min_window_width), float(self.window_height) / float(visualisation.min_window_height))  # calculate how much larger the window must be
                visualisation.font_size = int(visualisation.font_size / ratio)  # scale all dimensions
                self.object_width = int(self.object_width / ratio)
                self.object_depth = int(self.object_depth / ratio)
                self.vis_r = self.vis_r / ratio
                visualisation.top_bar_height = int(visualisation.top_bar_height / ratio)
                visualisation.bottom_bar_height = int(visualisation.bottom_bar_height / ratio)
                self.shelf_width = int(self.shelf_width / ratio)
                self.shelf_height = int(self.shelf_height / ratio)
                self.window_width = int(self.window_width / ratio)
                self.window_height = int(self.window_height / ratio)

                self.screen = pygame.display.set_mode((self.window_width, self.window_height))
                
                self.text_library = {}  # start a library with all text you will ever need
                text = pygame.font.Font(None, visualisation.font_size)
                self.text_library["World model"] = text.render("World model", True, visualisation.text_colour)

                self.empty_screen()
                pygame.display.flip()

        def empty_screen(self):
                self.screen.fill(visualisation.background_colour)  # show the empty world screen
                pygame.draw.rect(self.screen, visualisation.bar_colour, pygame.Rect(0, 0, self.window_width, visualisation.top_bar_height))
                pygame.draw.rect(self.screen, visualisation.bar_colour, pygame.Rect(0, self.window_height - visualisation.bottom_bar_height, self.window_width, visualisation.bottom_bar_height))
                self.screen.blit(self.text_library["World model"], (self.shelf_width / 2 - self.text_library["World model"].get_width() // 2, visualisation.top_bar_height / 2 - self.text_library["World model"].get_height() // 2))

        def real_pose_to_vis_pose(self, real_pose):  # returns the coordinate in the visualisation, based on the pose on the shelf
                vis_pose = [None, None]  # initialise the grid pose variable
                vis_pose[0] = int(round((real_pose[0] - self.object_width_dim / 2) * self.vis_r))
                vis_pose[1] = self.window_height - visualisation.bottom_bar_height - self.object_depth - int(round((real_pose[1] - self.object_depth_dim / 2) * self.vis_r))
                return vis_pose

        def add_object(self, pose, manipulability, detection_method):  # method to show a objects in the world model
                vis_pose = self.real_pose_to_vis_pose(pose)
                if not manipulability == False:
                        border_colour = visualisation.manipulable_object_border_colour
                else:
                        border_colour = visualisation.immovable_object_border_colour
                if detection_method == "sight":
                        fill_colour = visualisation.seen_object_colour
                elif detection_method == "inference":
                        fill_colour = visualisation.infered_object_colour
                elif detection_method == "action":
                        fill_colour = visualisation.action_object_colour
                else:
                        fill_colour = visualisation.unknown_detection_object_colour
                pygame.draw.rect(self.screen, border_colour, pygame.Rect(vis_pose[0], vis_pose[1], self.object_width, self.object_depth))
                pygame.draw.rect(self.screen, fill_colour, pygame.Rect(vis_pose[0] + visualisation.object_border_width, vis_pose[1] + visualisation.object_border_width, self.object_width - 2 * visualisation.object_border_width, self.object_depth - 2 * visualisation.object_border_width))

        def show(self, world_model, manipulabilities, detection_methods):  # method to show the shelves in world_model
                self.empty_screen()
                for i in range(len(world_model)):
                        self.add_object(world_model[i], manipulabilities[i], detection_methods[i])  # show all objects of the world model
                pygame.display.flip()

def retrieve_world_model():  # returns a list of poses of objects in the  world model
        wm_poses = []
        manips = []
        det_methods = []
        query = prolog.query("list_objects(L), member(O, L), object_at(O, [X, Y, T]), get_manipulability(O, M)")
        for solution in query.solutions():  # first add all objects, asif the detection method is unknown
                wm_poses.append(np.array([solution["X"], solution["Y"], solution["T"]]))
                manips.append(solution["M"])
                det_methods.append("unknown")
        query = prolog.query("list_objects(L), member(O, L), get_inference_detection(O, _), object_at(O, [X, Y, T]), get_manipulability(O, M)")
        for solution in query.solutions():  # then add the infered objects
                wm_poses.append(np.array([solution["X"], solution["Y"], solution["T"]]))
                manips.append(solution["M"])
                det_methods.append("inference")
        query = prolog.query("list_objects(L), member(O, L), get_last_seen_detection(O, _), object_at(O, [X, Y, T]), get_manipulability(O, M)")
        for solution in query.solutions():  # then put in the observed objects
                wm_poses.append(np.array([solution["X"], solution["Y"], solution["T"]]))
                manips.append(solution["M"])
                det_methods.append("sight")
        query = prolog.query("list_objects(L), member(O, L), get_action_detection(O, _), object_at(O, [X, Y, T]), get_manipulability(O, M)")
        for solution in query.solutions():  # lastly add the action detected objects
                wm_poses.append(np.array([solution["X"], solution["Y"], solution["T"]]))
                manips.append(solution["M"])
                det_methods.append("action")
        return np.array(wm_poses), manips, np.array(det_methods)

def new_shelf():
    dim_answer = []
    while not isinstance(dim_answer, dict):
        dim_answer = prolog.once("active_shelf(S), get_dimensions_in_objects(S, [SD, SW| _]), shelf_type(S, T), get_dimensions(T, [OD, OW| _]), rdf_has(S, poc:'hasInterRowDistance', literal(type(_, Da))), atom_number(Da, D)")
    vis = visualisation(dim_answer["SW"], dim_answer["SD"], dim_answer["OW"], dim_answer["OD"], dim_answer["D"])
    return vis

def main_function():
    rospy.init_node("world_model_visualisation")
    rate = rospy.Rate(visualisation.update_frequency)
    shelf_active = False  # assume no shelf is active
    while not rospy.is_shutdown():
        shelf_answer = prolog.once("active_shelf(_)")  # ask if a shelf is active
        if isinstance(shelf_answer, dict) and not shelf_active:  # if a shelf is active, but the visualisation did not know
                pygame.display.quit()  # close any still open visualisations
                pygame.quit()
                vis = new_shelf()  # open a new visualisation
                shelf_active = True  # register that a shelf is active
        if isinstance(shelf_answer, dict) and shelf_active:  # if a shelf is active and the visualisation knew a shelf was active
                world_model, manip, detec_methods = retrieve_world_model()
                vis.show(world_model, manip, detec_methods)
                pass
        if not isinstance(shelf_answer, dict) and not shelf_active:  # if there is no shelf active and the visualisation knew so
                pass  # don't do anything
        if not isinstance(shelf_answer, dict) and shelf_active:  # if there is no shelf active, but the visualisation thought there was
                shelf_active = False  # remember there is no shelf active
        rate.sleep()

if __name__ == "__main__":
    try:
        prolog = Prolog()  # start communication with rosprolog
        main_function()
    except rospy.ROSInterruptException:
        pass
#!/usr/bin/env python

import rospy
import random
import math

from mobile_action_primitives.product import *
from geometry_msgs.msg import Pose
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import (
    SpawnModel,
    DeleteModel,
)
from std_srvs.srv import *
from world_model_reasoner.srv import *

import roslib; roslib.load_manifest("rosprolog")
from rosprolog_client import PrologException, Prolog

def service_client(service_name, service_type, *args):  # function to call a service and returns the response
    rospy.wait_for_service(service_name)
    try:
        service_handle = rospy.ServiceProxy(service_name, service_type)
        return service_handle(*args)
    except rospy.ServiceException as e:
        rospy.loginfo("Service call failed: %s" %e)

def delete_object(name):  # Delete model of name from gazebo simulation
    try:
        delete_model = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)
        resp_delete = delete_model(name)
        return True
    except rospy.ServiceException, e:
        rospy.loginfo("Delete Model service call failed: {0}".format(e))

def clear_shelf():  # deletes all models with a name starting with "product_" from the simulation
    models = rospy.wait_for_message("/gazebo/model_states", ModelStates)
    success = True
    for name in models.name:
        if name.startswith("product_"):
            success = success and delete_object(name)
    return success  # success will be true if delete_object has succeeded every time

def generate_configuration():
    rospy.loginfo(rospy.get_name() + ": Generating new configuration")
    service_client("/world_model_communicator/start_shelf", string, shelf_name)
    answer = prolog.once("active_shelf(S), shelf_capacity(S, Cap), shelf_type(S, T), get_dimensions(T, [Depth, Width| _]), get_pose(S, [X, Y, _, Z| _]), pose_error_quotient(Q)")
    if isinstance(answer, dict):
        global shelf_pose
        shelf_pose.position.x = answer["X"]
        shelf_pose.position.y = answer["Y"]
        shelf_pose.position.z = answer["Z"]
        global configuration
        configuration = []  # empty the current configuration
        row_x = []  # list to save the x positions of the rows of the shelf
        query = prolog.query("row_pose(_, X)")  # get the x coordinates of the rows
        for solution in query.solutions():
            row_x.append(solution["X"])  # and save them
        objs_per_row = int(answer["Cap"] / len(row_x))
        x_noise = answer["Width"] / answer["Q"] * x_noise_factor  # amplitude of the variance on the pose in x direction
        exists = []
        for i in range(answer["Cap"]):  # for every object that could possibly be there
            exists.append(bool(random.getrandbits(1)))  # 50% chance of the object existing
        for i in range(answer["Cap"]):
            if exists[i]:
                configuration.append(Pose())
                configuration[-1].position.x = row_x[int(math.floor(i / objs_per_row))]
                if random.random() <= moved_x_probability:
                    configuration[-1].position.x += random.uniform(-x_noise, x_noise)  # add some random noice to the x pose
                configuration[-1].position.y = ((i % objs_per_row) + 0.5) * answer["Depth"]
                if not ((i + 1) % objs_per_row) == 0:  # if the product is not one at the back of the shelf
                    if not exists[i + 1]:  # and the product behind it does not exist
                        if random.random() < moved_y_probability:
                            configuration[-1].position.y += random.uniform(0, answer["Depth"])  # possibly move it back a bit
        rospy.loginfo(rospy.get_name() + ": Placing objects at:\n" + str(configuration))
        success = True
    else:
        success = False
    service_client("/world_model_communicator/stop_shelf", string, shelf_name)
    return success

def load_configuration():
    hagelslag = Product("hagelslag.urdf")
    hagelslag.product_shelf_pose = shelf_pose
    for i in range(len(configuration)):
        hagelslag.spawn_on_shelf(configuration[i])
    return True

def load_last_configuration_callback(req):
    rospy.loginfo(rospy.get_name() + ": Loading last configuration on " + shelf_name + ".")
    clear_shelf()
    load_configuration()
    return TriggerResponse(True, "")

def load_new_configuration_callback(req):
    rospy.loginfo(rospy.get_name() + ": Loading new configuration on " + shelf_name + ".")
    clear_shelf()
    success = generate_configuration()
    load_configuration()
    return TriggerResponse(success, "")

def set_random_seed_callback(req):
    rospy.loginfo(rospy.get_name() + ": set random seed to " + str(random_seed) + ".")
    random.seed(random_seed)
    return TriggerResponse(True, "")

def main_function():
    rospy.init_node("shelf_config_generator")

    rospy.Service("/shelf_config_generator/load_last_configuration", Trigger, load_last_configuration_callback)
    rospy.Service("/shelf_config_generator/load_new_configuration", Trigger, load_new_configuration_callback)
    rospy.Service("/shelf_config_generator/set_random_seed", Trigger, set_random_seed_callback)

    rospy.spin()

if __name__ == "__main__":
    try:

        random_seed = 2020  # seed for the random number generator
        shelf_name = "hagelslagShelf"
        x_noise_factor = 0.5  # factor over the amplitude of variations of the x position
        moved_x_probability = 1.0  # probability [0.0, 1.0] of a product being moved out of the grid in x direction
        moved_y_probability = 0.8  # probability [0.0, 1.0] of a product being moved out of the grid in y direction

        shelf_pose = Pose()
        configuration = []

        prolog = Prolog()  # start communication with rosprolog
        main_function()
    except rospy.ROSInterruptException:
        pass
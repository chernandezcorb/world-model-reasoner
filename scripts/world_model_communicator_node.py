#!/usr/bin/env python

import rospy
import os
import copy

import world_model_reasoner.msg
from geometry_msgs.msg import *
from gazebo_msgs.msg import ModelStates
from rosplan_dispatch_msgs.msg import CompletePlan
from std_srvs.srv import *
from world_model_reasoner.srv import *
from mobile_action_primitives.srv import *

import roslib; roslib.load_manifest("rosprolog")
from rosprolog_client import PrologException, Prolog

def service_client(service_name, service_type, *args):  # function to call a service and returns the response
    rospy.wait_for_service(service_name)
    try:
        service_handle = rospy.ServiceProxy(service_name, service_type)
        return service_handle(*args)
    except rospy.ServiceException as e:
        rospy.loginfo("Service call failed: %s" %e)

def start_shelf_callback(req):
    answer = prolog.once("start_shelf(poc:'" + req.data + "')")  # response is a dictionary if successful, an empty list if it fails
    resp = stringResponse()
    resp.success = isinstance(answer, dict)  # return if the shelf was successfully started
    if resp.success:
        rospy.loginfo(rospy.get_name() + ": started shelf " + req.data + ".")

        shelf_pose = set_shelf_poseRequest()
        shelf_pose_answer = prolog.once("get_pose(poc:'" + req.data + "', [X, Y, _, Z| _])")  # request the pose of the shelf from the ontology
        shelf_pose.pose.position.x = shelf_pose_answer["X"]
        shelf_pose.pose.position.y = shelf_pose_answer["Y"]
        shelf_pose.pose.position.z = shelf_pose_answer["Z"]
        shelf_pose_resp = service_client("/action_execution/set_shelf_pose", set_shelf_pose, shelf_pose)  # inform the action execution of the shelf to use
        resp.success = (resp.success and shelf_pose_resp.success)
        shelf_pose_resp = service_client("/object_detector/set_shelf_pose", set_shelf_pose, shelf_pose)  # inform the object detector of the shelf to use
        resp.success = (resp.success and shelf_pose_resp.success)

        shelf_dim = set_object_parametersRequest()
        shelf_dim_answer = prolog.once("get_dimensions(poc:'" + req.data + "', [D, W| _])")
        shelf_dim.depth = shelf_dim_answer["D"]
        shelf_dim.width = shelf_dim_answer["W"]
        shelf_dim_resp = service_client("/object_detector/set_shelf_parameters", set_object_parameters, shelf_dim)
        resp.success = (resp.success and shelf_dim_resp.success)

        obj_par = set_object_parametersRequest()
        object_dim_answer = prolog.once("shelf_type(poc:'" + req.data + "', T), get_dimensions(T, [D, W, H| _])")  # request the object dimensions from the ontology
        obj_par.depth = object_dim_answer["D"]
        obj_par.width = object_dim_answer["W"]
        obj_par.height =  object_dim_answer["H"]
        obj_par.grasp_height_factor = 0.85
        obj_par_resp = service_client("/action_execution/set_object_parameters", set_object_parameters, obj_par)  # inform the action execution of the object parameters to use
        resp.success = (resp.success and obj_par_resp.success)
        obj_par_resp = service_client("/object_detector/set_object_parameters", set_object_parameters, obj_par)  # inform the object detector of the object parameters to use
        resp.success = (resp.success and obj_par_resp.success)
    return resp

def stop_shelf_callback(req):
    answer = prolog.once("stop_shelf(poc:'" + req.data + "')")  # response is a dictionary if successful, an empty list if it fails
    resp = stringResponse()
    resp.success = isinstance(answer, dict)  # return if the shelf was successfully started
    if resp.success:
        rospy.loginfo(rospy.get_name() + ": stopped shelf " + req.data + ".")
    return resp

def first_detect_callback(req):
    detections = copy.deepcopy(last_detection)
    if assume_full_behind:
        # add some extra objects to the detection
        answer = prolog.once("active_shelf(S), get_dimensions(S, [SD, SW| _]), shelf_type(S, T), get_dimensions(T, [OD, OW| _])")  # query the object and shelf dimensions
        for i in range(len(detections.name)):  # for every observed object
            back_most_y = detections.pose[i].position.y  # add additional objects to detections
            while back_most_y <= answer["SD"] - answer["OD"]*3/2:  # as long as there fits an extra object behind object i
                back_most_y += answer["OD"]
                detections.name.append("product_" + str(detections.pose[i].position.x) + str(back_most_y))  # give the new product a name
                detections.pose.append(Pose())
                detections.pose[-1].position.x = detections.pose[i].position.x
                detections.pose[-1].position.y = back_most_y

    # register the objects as you normally would
    now = rospy.get_rostime()  # get the current time
    query_start = "seen_object_at(Obj, ["  # everything the query needs before the pose
    query_end = ", 0.0], [" + str(now.secs) + ", " + str(now.nsecs) + "])"  # everything the query needs after the pose
    resp = TriggerResponse()
    resp.success = True
    num_prod = len(detections.name)
    for i in range(num_prod):  # for every observed object
        query_string = query_start + str(detections.pose[i].position.x) + ", " + str(detections.pose[i].position.y) + query_end  # add the pose to the query
        answer = prolog.once(query_string)  # pose the query
        resp.success = resp.success and isinstance(answer, dict)  # return if the query succeeded
    if resp.success:  # if the query was successful
        rospy.loginfo(rospy.get_name() + ": Registered first detection")
    return resp

def detect_callback(req):  # if an object at that pose is known, don't do anything, otherwise, add the object and its pose and hard clear any intersecting objects
    now = rospy.get_rostime()  # get the current time
    query_start = "seen_object_at(Obj, ["  # everything the query needs before the pose
    query_end = ", 0.0], [" + str(now.secs) + ", " + str(now.nsecs) + "])"  # everything the query needs after the pose
    resp = TriggerResponse()
    resp.success = True
    num_prod = len(last_detection.name)
    for i in range(num_prod):  # for every observed object
        pose_string = str(last_detection.pose[i].position.x) + ", " + str(last_detection.pose[i].position.y)  # make a string of the pose
        query_string = query_start + pose_string + query_end  # build the query
        answer = prolog.once(query_string)  # register the object
        resp.success = resp.success and isinstance(answer, dict)  # return if the query succeeded
    if resp.success:  # if the query was successful
        rospy.loginfo(rospy.get_name() + ": Registered objects")
    prolog.once("forall((rdf_has(Det, rdf:type, poc:'Detection'), get_detection_method(Det, poc:'action')), retract_detection(Det))")  # retract all detections of method action
    return resp

def shelf_done_callback(req):
    answer = prolog.once("shelf_done")
    resp = TriggerResponse()
    resp.success = isinstance(answer, dict)
    if resp.success:
        rospy.loginfo(rospy.get_name() + ": I am done with this shelf.")
    return resp

def shelf_full_callback(req):
    answer = prolog.once("shelf_full")
    resp = TriggerResponse()
    resp.success = isinstance(answer, dict)
    if resp.success:
        rospy.loginfo(rospy.get_name() + ": This shelf is fully stocked.")
    else:
        rospy.loginfo(rospy.get_name() + ": This shelf is not fully stocked.")
    return resp

def get_action_callback(req):
    resp = response_actionResponse()
    rospy.loginfo(rospy.get_name() + ": Planning stocking using PDDL.")
    generate_PDDL() # Call function to generate PDDL problem file
    complete_plan = call_ROSPlan_client()  # request the plan from ROSPlan
    resp = retrieve_pddl_action(complete_plan.plan[0])  # Retrieve one action of the plan
    return resp

def generate_PDDL():
    # Collect information from the ontology
    shelf_dim = prolog.once("active_shelf(S), get_dimensions_in_objects(S, [NP, NR| _])")  # pose the query
    if not isinstance(shelf_dim, dict):  # if the query was successful
        rospy.loginfo(rospy.get_name() + ": Something went wrong querying the dimensions of the shelf in number of objects.")
        return ""
    num_prod = []  # initialise empty lists
    front_r = []
    block_r = []
    for r in range(shelf_dim["NR"]):
        nanswer = prolog.once("nobjs_inRow(N, " + str(r) + ")")
        num_prod.append(nanswer["N"])  # this is the first point where row numbers are linked to their actual location on the shelf
        fanswer = prolog.once("front_occupied(" + str(r) + ", Obj), get_manipulability(Obj, Manip)")  # ask Prolog if there is a object in the front of each row
        front_r.append(isinstance(fanswer, dict))
        if front_r[-1]:
            block_r.append(fanswer["Manip"] == False)
        else:
            block_r.append(False)
    # Generate the PDDL problem file
    file_path = os.path.dirname(os.path.realpath(__file__))
    file_path = file_path.replace("scripts", "pddl/")
    file_name = "generated_problem.pddl"
    gfile_name = file_path + file_name
    f = open(gfile_name, "w")  # open the problem file to write to
    f.write("(define ")  # start the pddl problem file
    f.write("(problem stockashelf) \n\n ")  # give the problem a name
    f.write("(:domain poc) \n\n ")  # define the used domain
    f.write("(:objects \n\n ")  # open the objects tag
    for r in range(shelf_dim["NR"]):
        f.write("r" + str(r) + " ")
    f.write(" - row \n ) \n\n ")  # close the objects tag
    f.write("(:init \n ")  # open the initial state tag
    f.write("(= (total-action-cost) 0) \n ")  # set the initial operation cost to 0
    f.write("(= (max-objects-per-row) ")  # open the shelf depth tag
    f.write(str(shelf_dim["NP"]) + ") \n ")  # close the shelf depth tag
    for r in range(shelf_dim["NR"]):
        f.write("(= (row-number-of-objects r" + str(r) + ") " + str(num_prod[r]) + ") \n ")
        if front_r[r]:  # if the front of this row is occupied
            f.write("(front-occupied r" + str(r) + ") \n ")
        if block_r[r]:  # if the front of this row is blocked
            f.write("(front-blocked r" + str(r) + ") \n ")
    f.write(") \n\n ")  # close the initial state tag
    f.write("(:goal \n (and \n ")  # open the goal tag
    f.write("(forall (?r - row) \n ")  # universal goal for all rows
    f.write("(or \n ")
    f.write("(= (row-number-of-objects ?r) (max-objects-per-row)) \n ")
    f.write("(and \n ")
    f.write("(front-occupied ?r) \n ")
    f.write("(front-blocked ?r) \n ")
    f.write(") \n ) \n ) \n ")
    f.write(") \n ) \n\n ")  # close the goal tag
    f.write("(:metric minimize (total-action-cost)) \n ")  # add the metric
    f.write(")")  # finish the pddl problem file
    f.close()  # close the pddl file
    rospy.loginfo(rospy.get_name() + ": Generated PDDL: " + gfile_name)
    return gfile_name

def call_ROSPlan_client():
    # call the planner
    rospy.wait_for_service('/rosplan_planner_interface/planning_server')
    planning_server = rospy.ServiceProxy('/rosplan_planner_interface/planning_server', Empty)
    planning_server()
    # parse the plan properly
    rospy.wait_for_service('/rosplan_parsing_interface/parse_plan')
    parsing_server = rospy.ServiceProxy('/rosplan_parsing_interface/parse_plan', Empty)
    parsing_server()
    # return the plan
    return rospy.wait_for_message("/rosplan_parsing_interface/complete_plan", CompletePlan)

def retrieve_pddl_action(plan_step):  # function that takes one step of the plan and returns the action in desired format
    resp = response_actionResponse()
    resp.action.type = plan_step.name  # get the type of action
    row_name = plan_step.parameters[0].value  # get the row that the action is applied to
    row_number = int(row_name.replace("r",""))
    canswer = prolog.once("row_pose(" + str(row_number) + ", X), front_Y(Y)")  # ask Prolog for the coordinates corresponding to the front of this row
    if not isinstance(canswer, dict):
        rospy.loginfo(rospy.get_name() + ": Something went wrong translating a row number to a pose.")
        return resp
    resp.action.poses.append(Pose())
    resp.action.poses[0].position.x = canswer["X"]  # enter this as the start pose
    resp.action.poses[0].position.y = canswer["Y"]
    if resp.action.type == "push-back":  # if the action is pushing
        danswer = prolog.once("active_shelf(S), shelf_type(S, T), get_dimensions(T, [PD| _])")
        if not isinstance(danswer, dict):
            rospy.loginfo(rospy.get_name() + ": Something went wrong requesting object depth.")
            return resp
        resp.action.poses.append(Pose())
        resp.action.poses[1].position.x = canswer["X"]  # end pose is one object depth behind the startpose
        resp.action.poses[1].position.y = canswer["Y"] + danswer["PD"]
    return resp

def register_action_callback(req):  # function to assert an action intent and success to the world model
    now = rospy.get_rostime()  # get the current time
    valid_action = False  # assume action is not valid
    answer = None  # initialise answer variable
    query_string = "action("  # open the query string
    if req.action.type == "place":  # planned_action([X, Y, Theta], Time)
        query_string += "[" + str(req.action.poses[0].position.x) + ", " + str(req.action.poses[0].position.y) + ", 0.0], "  # add the pose to the query
        query_string += "[" + str(now.secs) + ", " + str(now.nsecs) + "], "  # add the time to the query
        query_string += str(int(req.action.success)) + ")"  # add action success to the query and close it
        valid_action = True
    elif req.action.type.startswith("push"):  # planned_action([SX, SY, STheta], [EX, EY, ETheta], Time)
        query_string += "[" + str(req.action.poses[0].position.x) + ", " + str(req.action.poses[0].position.y) + ", 0.0], "  # add the start pose to the query
        query_string += "[" + str(req.action.poses[1].position.x) + ", " + str(req.action.poses[1].position.y) + ", 0.0], "  # add the start pose to the query
        query_string += "[" + str(now.secs) + ", " + str(now.nsecs) + "], "  # add the time to the query
        query_string += str(int(req.action.success)) + ")"  # add action success to the query and close it
        valid_action = True
    else:
        rospy.loginfo(rospy.get_name() + ": No valid action type: " + req.action.type)
    if valid_action:
        rospy.loginfo(rospy.get_name() + ": Registering action: " + query_string)
        answer = prolog.once(query_string)  # send the planned action to be asserted
    resp = request_actionResponse()
    resp.success = isinstance(answer, dict)  # return if the assertian succeeded
    return resp

def world_model_summary_callback(req):
    resp = WMSummaryResponse()
    query = prolog.query("list_objects(L), member(O, L), object_at(O, [X, Y| _]), get_manipulability(O, M)")
    for solution in query.solutions():
        resp.data.append(world_model_reasoner.msg.object())
        resp.data[-1].name = str(solution["O"].replace("http://www.tudelft.org/arthur/proof-of-concept/ontology#", ""))
        resp.data[-1].pose = Pose()
        resp.data[-1].pose.position.x = solution["X"]
        resp.data[-1].pose.position.y = solution["Y"]
        resp.data[-1].manipulability = str(solution["M"])
    return resp

def detection_callback(data):
    global last_detection
    last_detection = data

def assume_full_callback(req):
    global assume_full_behind
    assume_full_behind = req.data
    rospy.loginfo("%s: Set assuming full behind to %s" % (rospy.get_name(), str(assume_full_behind)))
    resp = SetBoolResponse(True, "")
    return resp

def main_function():
    rospy.init_node("world_model_communicator")

    rospy.Service("/world_model_communicator/detect", Trigger, detect_callback)
    rospy.Service("/world_model_communicator/first_detect", Trigger, first_detect_callback)
    rospy.Service("/world_model_communicator/start_shelf", string, start_shelf_callback)
    rospy.Service("/world_model_communicator/stop_shelf", string, stop_shelf_callback)
    rospy.Service("/world_model_communicator/shelf_done", Trigger, shelf_done_callback)
    rospy.Service("/world_model_communicator/shelf_full", Trigger, shelf_full_callback)
    rospy.Service("/world_model_communicator/get_action", response_action, get_action_callback)
    rospy.Service("/world_model_communicator/register_action", request_action, register_action_callback)
    rospy.Service("/world_model_communicator/world_model_summary", WMSummary, world_model_summary_callback)
    rospy.Service("/world_model_communicator/assume_full", SetBool, assume_full_callback)

    rospy.Subscriber("/object_detector/detection", ModelStates, detection_callback)

    rospy.spin()

if __name__ == "__main__":
    try:
        assume_full_behind = False  # setting to assume if everything behind the first observed objects is full or not

        last_detection = ModelStates()  # global variable to store the last detection sent by the object_detector
        prolog = Prolog()  # start communication with rosprolog
        main_function()
    except rospy.ROSInterruptException:
        pass
#!/usr/bin/env python

from visualisation import *
from geometry_msgs.msg import Pose

shelf_width_o = 1
shelf_depth_o = 3
object_width = 0.072
object_depth = 0.072
inter_row_distance = 0.09

# save_location = "Documents/Experiments/Empty_shelf/imgs/"
# save_location = "Pictures/"
# file_name = "Reasoning_0.jpeg"

poses = []
# poses.append([0.081, 0.036])
poses.append([0.081, 0.108])
poses.append([0.081, 0.180])
# poses.append([0.243, 0.036])
# poses.append([0.243, 0.108])
# poses.append([0.243, 0.180])

manipulabilities = []
manipulabilities.append(True)
manipulabilities.append(True)
manipulabilities.append(True)
# manipulabilities.append(True)
# manipulabilities.append(True)
# manipulabilities.append(True)

detection_methods = []
detection_methods.append("sight")
detection_methods.append("sight")
# detection_methods.append("inference")
# detection_methods.append("x")
# detection_methods.append("x")
# detection_methods.append("x")

vis = visualisation(shelf_width_o, shelf_depth_o, object_width, object_depth, inter_row_distance)

vis.show(poses, manipulabilities, detection_methods)

raw_input()

# pygame.image.save(vis.screen, save_location + file_name)

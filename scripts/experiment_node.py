#!/usr/bin/env python

import rospy
import actionlib
import time

from std_msgs.msg import *
from world_model_reasoner.msg import *
from gazebo_msgs.msg import ModelStates
from std_srvs.srv import *

def service_client(service_name, service_type, *args):  # function to call a service and returns the response
    rospy.wait_for_service(service_name)
    try:
        service_handle = rospy.ServiceProxy(service_name, service_type)
        return service_handle(*args)
    except rospy.ServiceException as e:
        rospy.loginfo("Service call failed: %s" %e)

def shelf_active_callback(data):
    global shelf_active
    shelf_active = data.data

def stocking_client(shelf_name):
    result = StockShelfResult()
    result.success = False  # assume failure
    client = actionlib.SimpleActionClient("StockShelf", StockShelfAction)
    client.wait_for_server()
    goal = StockShelfGoal(shelf_name)
    client.send_goal(goal)
    # During action execution:
    client.wait_for_result()
    # When finished or out of time:
    if client.get_result() is not None:
        result = client.get_result()
    return result

def main_function():
    rospy.init_node("experiment_node")

    rospy.Subscriber("/stocker_node/shelf_active", Bool, shelf_active_callback)

    # service_client("/shelf_config_generator/set_random_seed", Trigger)  # set the random seed at the start

    shelf_counter = 0  # count the amount of shelves stocked

    while (not rospy.is_shutdown()) and shelf_counter < num_shelves_to_stock:

        service_client("/stocker_node/use_reasoner", SetBool, SetBoolRequest(True))  # use the reasoner
        rospy.loginfo(rospy.get_name() + ": Starting with the reasoner on shelf number " + str(shelf_counter + 1) + ".")
        service_client("/shelf_config_generator/load_new_configuration", Trigger)  # load a new configuration
        stock_resp = stocking_client("hagelslagShelf")  # stock the shelf

        # service_client("/stocker_node/use_reasoner", SetBool, SetBoolRequest(False))  # don't use the reasoner
        # rospy.loginfo(rospy.get_name() + ": Starting without the reasoner on shelf number " + str(shelf_counter + 1) + ".")
        # service_client("/shelf_config_generator/load_last_configuration", Trigger)  # load a new configuration
        # stock_resp = stocking_client("hagelslagShelf")  # stock the shelf

        shelf_counter += 1
        models = rospy.wait_for_message("/gazebo/model_states", ModelStates),
        num_products = 0
        for name in models[0].name:
            if name.startswith("product_"):
                num_products += 1  # count the number of products in the scene
        if stock_resp.file_name != "":  # if a file name was supplied
            file = open(stock_resp.file_name, "a")  # open the result file to append to
            file.write("\n\n")
            file.write("Number of objects in the scene: %s\n" % str(num_products))
            file.close()
        while shelf_active:  # wait until the ontology knows the shelf is no longer being stocked to prevent Gazebo from crashing
            time.sleep(0.1)

if __name__ == "__main__":
    try:
        num_shelves_to_stock = 3

        shelf_active = False  # global to remember if a shelf is currently active according to the ontology
        main_function()
    except rospy.ROSInterruptException:
        pass
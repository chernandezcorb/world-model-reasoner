#!/usr/bin/env python

import rospy
import actionlib
import copy
import time
import os
import datetime

from std_msgs.msg import *
from world_model_reasoner.msg import *
from gazebo_msgs.msg import ModelStates
from std_srvs.srv import *
from world_model_reasoner.srv import *

def service_client(service_name, service_type, *args):  # function to call a service and returns the response
    rospy.wait_for_service(service_name)
    try:
        service_handle = rospy.ServiceProxy(service_name, service_type)
        return service_handle(*args)
    except rospy.ServiceException as e:
        rospy.loginfo("Service call failed: %s" %e)

class StockShelfActionObject(object):
    _feedback = StockShelfFeedback()
    _result = StockShelfResult()

    def __init__(self, save_to_file):
        self._action_name = "StockShelf"
        self._save_to_file = save_to_file
        self.wm_node = "world_model_reasoner"
        # self._max_stocking_time = 250  # if real time for stocking a shelf exceeds this time, the action is aborted (seconds)
        self._as = actionlib.SimpleActionServer(self._action_name, StockShelfAction, execute_cb = self.execute_cb, auto_start = False)
        self._as.start()

    def execute_cb(self, goal):
        preempted = False
        action_start_time = rospy.get_rostime().secs  # save the time when starting stocking in seconds

        if self._save_to_file:
            file_path = os.path.dirname(os.path.realpath(__file__))
            file_path = file_path.replace("scripts", "records/")
            file_name = experiment_name + "_experiment_feedback_" + str(datetime.datetime.now()) + ".txt"
            gfile_name = file_path + file_name
            feedback_file = open(gfile_name, "a")  # open the problem file to append to
            feedback_file.write("Starting stocking of shelf %s. \n" % goal.shelf_name)

        rospy.loginfo("%s: executing action %s on shelf %s." % (rospy.get_name(), self._action_name, goal.shelf_name))
        service_client(self.wm_node + "/start_shelf", string, goal.shelf_name)
        shelf_active_pub.publish(True)
        service_client(self.wm_node + "/first_detect", Trigger)
        loop_start_time = rospy.Time.now()
        planning_start_time = rospy.Time.now()
        while not rospy.is_shutdown():
            service_client(self.wm_node + "/detect", Trigger)
            shelf_done_resp = service_client(self.wm_node + "/shelf_done", Trigger)
            if shelf_done_resp.success:
                planning_end_time = rospy.Time.now()  # this is the end of the loop
                loop_end_time = rospy.Time.now()  # this is the end of the loop
                # Register the feedback and publish it
                self._feedback.planning_time = planning_end_time.to_sec() - planning_start_time.to_sec()  # planning is from registering the previous action until having decided on the next action
                self._feedback.loop_time = loop_end_time.to_sec() - loop_start_time.to_sec()  # loop is from registering the previous action until having executed the next action
                # self._feedback.configuration = rospy.wait_for_message("/object_detector/all_objects", ModelStates)
                wmsummary = service_client(self.wm_node + "/world_model_summary", WMSummary)
                self._feedback.world_model = wmsummary.data
                self._feedback.selected_action = action()
                self._as.publish_feedback(self._feedback)  # publish the last feedback
                break
            suggested_action = service_client(self.wm_node + "/get_action", response_action)
            planning_end_time = rospy.Time.now()  # this is the end of the planning phase
            req_action = request_actionRequest()
            req_action.action = copy.deepcopy(suggested_action.action)
            
            action_result = service_client("action_execution/perform", request_action, req_action)
            req_action.action.success = action_result.success

            # print "Perform action: "
            # print req_action
            # print "Enter y if the actions succeeded, n if it failed. Then press Enter."
            # answer = raw_input()
            # req_action.action.success = answer.startswith("y")

            loop_end_time = rospy.Time.now()  # this is the end of the loop

            # Register the feedback and publish it
            self._feedback.planning_time = planning_end_time.to_sec() - planning_start_time.to_sec()  # planning is from registering the previous action until having decided on the next action
            self._feedback.loop_time = loop_end_time.to_sec() - loop_start_time.to_sec()  # loop is from registering the previous action until having executed the next action
            # self._feedback.configuration = rospy.wait_for_message("/object_detector/all_objects", ModelStates)
            wmsummary = service_client(self.wm_node + "/world_model_summary", WMSummary)
            self._feedback.world_model = wmsummary.data
            self._feedback.selected_action = req_action.action
            self._as.publish_feedback(self._feedback)  # publish the feedback

            if self._save_to_file:
                feedback_file.write(str(self._feedback) + "\n")  # write the feedback to a file
                if self._save_to_file:
                    feedback_file.write("Action preempted. \n")  # write to the file

            if self._as.is_preempt_requested():  # if a preempt is requested
                rospy.loginfo("%s: Action %s preemted." % (rospy.get_name(), self._action_name))
                self._as.set_preempted()
                preempted = True
                break
            # if rospy.get_rostime().secs >= action_start_time + self._max_stocking_time:  # if the maximum stocking time is exceeded
            #     rospy.loginfo("%s: Stocking has exceeded the time limit." % rospy.get_name())
            #     self._as.set_preempted()
            #     preempted = True
            #     break

            loop_start_time = rospy.Time.now()  # this is the start of the loop and the planning phase
            planning_start_time = rospy.Time.now()
            service_client(self.wm_node + "/register_action", request_action, req_action)  # register action and success to the ontology

        if not preempted:  # if the action completed
            shelf_full_resp = service_client(self.wm_node + "/shelf_full", Trigger)
            if shelf_full_resp.success:  # if the shelf is actually full
                self._result.success = True  # return success
            else:
                self._result.success = False  # return false if no more moves where left available
            if self._save_to_file:
                self._result.file_name = gfile_name
            else:  # if no data was saved
                self._result.file_name = ""  # send no file name
            rospy.loginfo("%s: Action %s completed with result: %s" % (rospy.get_name(), self._action_name, self._result.success))
            self._as.set_succeeded(self._result)  # result says if the reasoner thinks it succeeded in stocking the shelf
            if self._save_to_file:
                feedback_file.write("Result:\n" + str(self._result) + "\n")  # write the result to a file

        if self._save_to_file:
            feedback_file.write("Finished stocking of shelf %s. \n" % goal.shelf_name)
            feedback_file.close()  # close the feedback file

        time.sleep(1)  # sleep one second to make sure the world model visualisation is updated before it is closed
        service_client(self.wm_node + "/stop_shelf", string, goal.shelf_name)
        shelf_active_pub.publish(False)

def main_function():
    rospy.init_node("stocker_node")

    shelf_active_pub.publish(False)

    server = StockShelfActionObject(save_to_file)

    rospy.spin()

if __name__ == "__main__":
    try:
        save_to_file = True
        experiment_name = "3.Partially_full_shelf"

        shelf_active_pub = rospy.Publisher("/stocker_node/shelf_active", Bool, queue_size = 10)

        main_function()
    except rospy.ROSInterruptException:
        pass
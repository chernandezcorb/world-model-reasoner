:- module(detections,[
	add_detection/3,
	update_detection_method/2,
	update_detection_time/2,
	update_detection/3,
	get_detections/2,
	get_detection_method/2,
	get_detection_time/2,
	get_last_seen_detection/2,
	get_action_detection/2,
	get_inference_detection/2,
	retract_detection/1
	]).

:- use_module(library('poc_module')).

:- rdf_meta
	add_detection(r, r, r),
	update_detection_method(r, r),
	update_detection_time(r, +),
	update_detection(r, r, +),
	get_detections(r, +),
	get_detection_method(r, r),
	get_detection_time(r, +),
	get_last_seen_detection(r, r),
	get_action_detection(r, r),
	get_inference_detection(r, r),
	retract_detection(r).

%%%%%%%%%%%%%%%%% Detection things %%%%%%%%%%%%%%%%%

add_detection(Object, Method, Detection):-  % add a detection through Method to Object
	physical_object(Object),  % Object needs to be an object
	rdf_resource(Method),
	rdfs_individual_of(Method, poc:'DetectionMethod'),  % Method must be a detection method
	rdf_instance_from_class(poc:'Detection', Detection),  % create a detection event
	rdf_instance_from_class(poc:'TimePoint', DetTP),  % create a detection time point
	rdf_assert(Object, poc:'hasDetection', Detection),  % link Detection to Object
	rdf_assert(Detection, poc:'hasDetectionMethod', Method),  % link Method to Detection
	rdf_assert(Detection, poc:'hasTimePoint', DetTP), !.  % link DetTP to Detection

update_detection_method(Detection, Method):-  % update the detection method of Detection to Method
	rdf_resource(Detection),
	rdfs_individual_of(Detection, poc:'Detection'),  % Detection needs to be a detection
	rdf_resource(Method),
	rdfs_individual_of(Method, poc:'DetectionMethod'),  % Method needs to be a detection method
	rdf_retractall(Detection, poc:'hasDetectionMethod', _),  % remove the triple about the detection method
	rdf_assert(Detection, poc:'hasDetectionMethod', Method), !.

update_detection_time(Detection, [Secs, Nsecs]):-  % update the time of Detection to UNIX time of Secs seconds and Nsecs nanoseconds
	rdf_resource(Detection),
	rdfs_individual_of(Detection, poc:'Detection'),  % Detection needs to be a detection
	number(Secs), number(Nsecs),  % the time needs to consist of numbers
	rdf_has(Detection, poc:'hasTimePoint', TP),  % get the time point of this detection
	rdf_retractall(TP, poc:'hasUnixTime', _),
	rdf_retractall(TP, poc:'hasnsecs', _),
	rdf_assert(TP, poc:'hasUnixTime', literal(type(xsd:long, Secs))),  % replace the seconds of the time
	rdf_assert(TP, poc:'hasnsecs', literal(type(xsd:long, Nsecs))), !.  % replace the nanoseconds of the time

update_detection(Detection, Method, Time):-  % update the method and time of a detection
	update_detection_method(Detection, Method),
	update_detection_time(Detection, Time).

get_detections(Object, Detections):-  % return list Detections of detection elements related to Object via 'hasDetection'
	physical_object(Object),  % Object must be an object
	findall(X, rdf_has(Object, poc:'hasDetection', X), Detections).

get_detection_method(Detection, Method):-  % return the Method belonging to Detection
	rdf_resource(Detection),
	rdfs_individual_of(Detection, poc:'Detection'),  % Detection must be a detection
	rdf_has(Detection, poc:'hasDetectionMethod', Method), !.

get_detection_time(Detection, [Secs, NSecs]):-
	rdf_resource(Detection),
	rdfs_individual_of(Detection, poc:'Detection'),  % Detection must be a detection
	rdf_has(Detection, poc:'hasTimePoint', TP),  % get the TimePoint of detection
	rdf_has(TP, poc:'hasUnixTime', literal(type(_, Secs))),  % retrieve the time
	rdf_has(TP, poc:'hasnsecs', literal(type(_, NSecs))), !.

get_last_seen_detection(Object, Detection):-  % returns the detection Object has last been seen
	physical_object(Object),  % Object must be an object
	get_detections(Object, Detections),  % get all its detections
	member(Detection, Detections),
	get_detection_method(Detection, poc:'sight'), !.  % find a detection with method 'sight'

get_action_detection(Object, Det):-  % succeeds if Object has a detection with method 'action'
	get_detections(Object, Detections),
	member(Det, Detections),
	get_detection_method(Det, poc:'action').

get_inference_detection(Object, Det):-  % succeeds if Object has a detection with method 'inference'
	get_detections(Object, Detections),
	member(Det, Detections),
	get_detection_method(Det, poc:'inference').

retract_detection(Det):-  % retracts a detection and its TimePoint from the ontology
	rdf_has(Det, poc:'hasTimePoint', TP),
	rdf_retractall(TP, _, _),
	rdf_retractall(_, _, TP),
	rdf_retractall(Det, _, _),
	rdf_retractall(_, _, Det).
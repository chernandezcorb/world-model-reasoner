:- module(actions,[
	action/3,
	action/4,
	push_back_object/3,
	push_side_object/3,
	object_out_of_row/1,
	fixing_action/3
	]).

:- use_module(library('poc_module')).
:- use_module(library('comp_spatial')).
:- use_module(library('ask_world_model')).
:- use_module(library('tell_world_model')).
:- use_module(library('detections')).

:-rdf_meta
	push_back_object(r, +, +),
	push_side_object(r, +, +),
	object_out_of_row(r),
	fixing_action(r, +, +).

action(Pose, Time, Success):-  % an action with one pose must be a placement action
	Success > 0,  % The action must have succeeded
	check_valid_pose(Pose),  % pose must consist of numbers
	\+object_at(_, Pose),  % there must be no object at Pose
	new_object(Object),  % add a new object
	update_pose(Object, Pose),  % give it its pose
	add_detection(Object, poc:'action', Detection),  % register that it is there due to an action
	update_detection_time(Detection, Time), !.  % register when it has been placed

action([SX, SY, STheta], [SX, EY, STheta], Time, Success):-  % an action with two poses must be a relocation action (push)
	Success > 0,  % The action must have succeeded
	number(SX), number(SY), number(STheta),  % poses must consist of numbers
	number(EY),
	object_at(Object, [SX, SY| _]),  % get the object Object that is being pushed
	roundD(EY - SY, DY),
	push_back_object(Object, [DY], Time), !.
action([SX, SY, STheta], [EX, SY, STheta], Time, Success):-  % an action with two poses must be a relocation action (push)
	Success > 0,  % The action must have succeeded
	number(SX), number(SY), number(STheta),  % poses must consist of numbers
	number(EX),
	object_at(Object, [SX, SY| _]),  % get the object Object that is being pushed
	roundD(EX - SX, DX),
	push_side_object(Object, [DX], Time), !.
action([SX, SY, _], _, Time, Success):-  % an action with two poses must be a relocation action (push)
	Success =< 0,  % In case the pushing action failed
	number(SX), number(SY),  % poses must consist of numbers
	object_at(Object, [SX, SY| _]),  % get the object Object that is being pushed
	update_manipulability(Object, false),  % the action probably failed due to the object being blocked, so mark it as such
	add_detection(Object, poc:'action', Detection),  % register that the object is there due to an action
	update_detection_time(Detection, Time), !.  % register when it has been moved

push_back_object(_, [DY], _):-
	DY =:= 0, !.  % pushing distance is 0
push_back_object(Object, [DY], Time):-
	obj_behind(BO, Object),  % there is an object behind Object
	object_at(Object, [_, FY| _]),  % get the Y of the front object Object
	object_at(BO, [_, BY| _]),  % get the Y of the back object BO
	get_dimensions(Object, [Depth| _]),
	roundD(FY + Depth + DY - BY, BDY),  % get the difference the back object would have to be pushed
	BDY > 0, !,  % if this pushing distance is positive
	push_back_object(BO, [BDY], Time),  % push that object
	push_back_object(Object, [DY], Time), !.  % then try to push Object again
push_back_object(Object, [DY], Time):-
	object_at(Object, [X, Y, Theta]),
	NY is Y + DY,
	update_pose(Object, [X, NY, Theta]),  % update the pose
	add_detection(Object, poc:'action', Detection),  % register that it is there due to an action
	update_detection_time(Detection, Time), !.  % register when it has been moved

push_side_object(_, [DX], _):-
	DX =:= 0, !.  % pushing distance is 0
push_side_object(Object, [DX], Time):-
	DX > 0,  % pushing to the right (DX = XEnd - XStart)
	object_at(Object, [X, Y, Theta]),
	forall(obj_toTheRightOf(RObj, Object),  % all objects to the right of Object must be further than DX away
		(object_at(RObj, [RX| _]),
		roundD(RX - X - DX, NDX),
		NDX =< 0)
	),
	NX is X + DX,
	update_pose(Object, [NX, Y, Theta]),  % update the pose
	add_detection(Object, poc:'action', Detection),  % register that it is there due to an action
	update_detection_time(Detection, Time), !.  % register when it has been moved
push_side_object(Object, [DX], Time):-
	DX > 0,  % pushing to the right (DX = XEnd - XStart)
	objs_toTheRightOf(RObjs, Object),  % there is a object to the right of Object
	member(RObj, RObjs),  % get one of the objects right of Object
	object_at(Object, [OX| _]),
	object_at(RObj, [RX| _]),
	roundD(RX - OX - DX, NDX),
	NDX > 0,  % if RObj is closer to Object than DX
	push_side_object(RObj, [NDX], Time),  % push RObj
	push_side_object(Object, [DX], Time), !.  % then try to push Object again
push_side_object(Object, [DX], Time):-
	DX < 0,  % pushing to the left (DX = XEnd - XStart)
	object_at(Object, [X, Y, Theta]),
	forall(obj_toTheLeftOf(LObj, Object),  % all objects to the left of Object must be further than DX away
		(object_at(LObj, [LX| _]),
		roundD(X - LX + DX, NDX),
		NDX >= 0)
	),
	NX is X + DX,
	update_pose(Object, [NX, Y, Theta]),  % update the pose
	add_detection(Object, poc:'action', Detection),  % register that it is there due to an action
	update_detection_time(Detection, Time), !.  % register when it has been moved
push_side_object(Object, [DX], Time):-
	DX < 0,  % pushing to the left (DX = XEnd - XStart)
	objs_toTheLeftOf(LObjs, Object),  % there is a object to the left of Object
	member(LObj, LObjs),  % get one of the objects left of Object
	object_at(Object, [OX| _]),
	object_at(LObj, [LX| _]),
	roundD(OX - LX + DX, NDX),
	NDX < 0,  % if LObj is closer to Object than DX
	push_side_object(LObj, [NDX], Time),  % push LObj
	push_side_object(Object, [DX], Time), !.  % then try to push Object again

object_out_of_row(Object):-  % returns an object that is not in a row
	rdf_has(Object, rdf:'type', poc:'Object'),  % get any object
	forall( row_pose(_, X),  % for the X coordinate of any row
		\+object_at(Object, [X| _])).  % Object is not at it

fixing_action(Object, [XStart, YStart, TStart], [XEnd, YStart, TStart]):-  % returns a starting and ending pose for an action that will move an object into a row  (Y and Theta are unchanged)
	findall([Obj, Y], (object_out_of_row(Obj), object_at(Obj, [_, Y| _])), ObjYs),  % find all objects that are out of row and there Y coordinates
	member([Object, YStart], ObjYs),  % get an object from the list
	forall(member([_, Y], ObjYs), YStart =< Y),  % YStart needs to be the smallest Y in the list
	object_at(Object, [XStart, YStart, TStart]),  % get Object's current X coordinate
	get_dimensions(Object, [Depth| _]),
	YStart =< Depth, !,  % Object needs to be close to the front of the shelf
	row_pose(_, XEnd),  % get the X coodinate of a row
	get_dimensions(Object, [_, Width| _]),  % get the width of Object
	active_shelf(Shelf),
	rdf_has(Shelf, poc:'hasInterRowDistance', literal(type(_, Da)), atom_number(Da, D)),
	roundD(abs(XStart - XEnd) * 2, DDX),  % the row must be the closest row, so
	DDX =< Width + D.  % the pushing distance must be smaller than half the row width

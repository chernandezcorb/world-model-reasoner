:- module(observations,[
	first_observation/2,
	new_observation/2,
	seen_object_at/3,
	clear_intersecting_objects/1,
	%% clear_intersecting_objects_hard/1,
	clear_line_of_sight_to/1,
	last_observed_before/2
	]).

:- use_module(library('poc_module')).
:- use_module(library('comp_spatial')).
:- use_module(library('ask_world_model')).
:- use_module(library('auto_update_world_model')).
:- use_module(library('detections')).

:- rdf_meta
	seen_object_at(r, +, +),
	clear_intersecting_objects(r),
	%% clear_intersecting_objects_hard(r),
	clear_line_of_sight_to(r),
	last_observed_before(r, +).

%%%%%%%%%%%%%%%%% Observation things %%%%%%%%%%%%%%%%%

first_observation(Poses, Time):-  % To call when a new shelf is observed for the first time. Pass a list of Poses of the objects observed and the Time of observation
	forall(member(Pose, Poses),  % for all observed poses
		(seen_object_at(Object, Pose, Time),  % report that an object has been seen there
		assume_behind(Object, Time))  % assume the presence of objects behind Object
	),
	auto_update(Time), !.

new_observation(Poses, Time):-  % To call when a new observation has been made. Pass a list of Poses of the objects observed and the Time of observation
	forall(member(Pose, Poses),  % for all observed poses
		seen_object_at(_, Pose, Time)),  % report that an object has been seen there
	forall((rdf_has(Det, rdf:type, poc:'Detection'), get_detection_method(Det, poc:'action')),  % for all detections with method 'action'
		retract_detection(Det)),  % retract all triples relating to that detection
	auto_update(Time), !.

seen_object_at(Object, Pose, Time):-  % When a object is seen and has been seen before
	check_valid_pose(Pose),
	object_at(Object, Pose),  % check if there is already a object at Pose
	(get_last_seen_detection(Object, Det) ;  % get the detection that should be replaced
		get_action_detection(Object, Det) ; 
		get_inference_detection(Object, Det);
		add_detection(Object, poc:'sight', Det)),
	update_detection(Det, poc:'sight', Time),  % register that it has been seen and update its detection time
	update_pose(Object, Pose),  % update the pose to trust the last detection.
	clear_line_of_sight_to(Object),  % make sure there is line of sight in the world model to Object
	!.
seen_object_at(Object, Pose, Time):-  % When a completely new object is seen
	check_valid_pose(Pose),
	new_object(Object),  % add a new object
	update_pose(Object, Pose),  % give it its pose
	add_detection(Object, poc:'sight', Detection),  % register that is has been seen
	update_detection_time(Detection, Time), !,  % register when it has been seen
	clear_intersecting_objects(Object),
	clear_line_of_sight_to(Object).  % make sure there is line of sight in the world model to Object

clear_intersecting_objects(Object):-  % removes all objects that intersect with Object and have last been seen before Object
	\+obj_intersectingObject(_, Object), !.
clear_intersecting_objects(Object):-  % if there is an object intersecting Object, which has been seen more recently than Object, Object should be removed
	obj_intersectingObject(IntO, Object),  % get an object to remove
	get_last_seen_detection(IntO, IOD),
	get_detection_time(IOD, Time),  % retrieve the time IntO was last seen
	last_observed_before(Object, Time),  % Object needs to have been observed for the last time before IntO
	retract_object(Object), !.  % remove Object
clear_intersecting_objects(Object):-
	obj_intersectingObject(IntO, Object),  % get an object to remove
	get_last_seen_detection(Object, OD),
	get_detection_time(OD, Time),  % retrieve the time Object was last seen
	last_observed_before(IntO, Time),  % IntO needs to have been observed last before Object
	retract_object(IntO),  % remove this intersecting object
	clear_intersecting_objects(Object).  % continue removing intersecting objects

%% clear_intersecting_objects_hard(Object):-  % removes all objects intersecting Object, not checking for detection method or time
%% 	\+obj_intersectingObject(_, Object), !.
%% clear_intersecting_objects_hard(Object):-
%% 	obj_intersectingObject(IntO, Object),
%% 	retract_object(IntO),
%% 	clear_intersecting_objects_hard(Object).

% Bug: This currently removes all past seen objects in front of Object.
% In a situation like:
%	| S |
%	  | Sp|
%	  | S |
% where S means seen_now and Sp means seen_past, the Sp object will be removed.
clear_line_of_sight_to(Object):-  % Make sure there is a line of sight from the front to Object
	\+obj_inFrontOf(_, Object), !.  % There is line of sight if there are no objects in front
clear_line_of_sight_to(Object):-
	get_last_seen_detection(Object, Detection),
	get_detection_time(Detection, Time),  % retrieve the last time this object was seen
	objs_inFrontOf(Objects, Object),  % Look for objects in front of this object
	forall((
		member(X, Objects),  % for every object in front
		last_observed_before(X, Time)),  % if X has last been observed before Time
	  retract_object(X)).  % remove everything you knew about this object

last_observed_before(Object, Time):-  % True if Object has last been seen before Time or only has been infered
	physical_object(Object),  % Object is an object
	get_last_seen_detection(Object, Det),
	get_detection_time(Det, LastTime),  % retrieve when it has last been seen
	time_before(LastTime, Time), !.  % LastTime is before Time
last_observed_before(Object, _):-
	get_inference_detection(Object, _).  % Object has a detection of method inference
last_observed_before(Object, _):-
	get_action_detection(Object, _).  % Object has a detection of method action

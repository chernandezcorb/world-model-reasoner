:- module(ask_world_model,[
	list_objects/1,
	list_shelves/1,

	physical_object/1,
	get_dimensions/2,
	get_dimensions_in_objects/2,
	get_pose/2,
	get_manipulability/2,
	shelf_capacity/2,
	shelf_type/2,
	active_shelf/1,
	no_moves_left/0,
	shelf_full/0,
	shelf_done/0,
	object_at/2,
	row_pose/2,
	obj_inRow/2,
	objs_inRow/2,
	nobjs_inRow/2,
	front_occupied/2,
	front_Y/1
	]).

:- use_module(library('poc_module')).
:- use_module(library('comp_spatial')).


:- rdf_meta
	physical_object(r),
	get_dimensions(r, t),
	get_dimensions_in_objects(r, t),
	get_pose(r, +),
	get_manipulability(r, +),
	shelf_capacity(r, t),
	shelf_type(r, r),
	object_at(r, +),
	obj_inRow(r, t),
	objs_inRow(r, +),
	front_occupied(+, r).

%%%%%%%%%%%%%%%%% List things %%%%%%%%%%%%%%%%%

list_objects(List):-
	active_shelf(Shelf),
	findall(P,
		(rdfs_individual_of(P, poc:'Object'),  % find all objects
		 rdf_has(P, poc:'onShelf', Shelf)),  % that are on the currently active shelf
		List).

list_shelves(List):-
	findall(S,
		rdfs_individual_of(S, poc:'Shelf'),
		List).

%%%%%%%%%%%%%%%%% Ask things %%%%%%%%%%%%%%%%%

physical_object(Thing) :-
  rdf_resource(Thing),  % Thing is a resource
  rdfs_individual_of(Thing, poc:'Object').  % and Thing is an Object

get_dimensions(S, [Depth, Width, Height]):- % get dimensions of Objects
	(physical_object(S)) ->  % if S in an Object, get the dimensions of its type
		(rdf_has(S, poc:'hasType', S2),
		get_dimensions(S2, [Depth, Width, Height]),
		!) ; 
		fail.
get_dimensions(S, [Depth, Width, Height]):-  % get dimensions of shelves or object types
	rdf_resource(S),  % S must be a resource
	((rdf_has(S, poc:'hasDepth', literal(type(_, D))), atom_number(D, DepthL), roundD(DepthL, Depth)) ; Depth = 'unknown'),  % round to solve floating point inaccuracy
	((rdf_has(S, poc:'hasWidth', literal(type(_, W))), atom_number(W, WidthL), roundD(WidthL, Width)) ; Width = 'unknown'),
	((rdf_has(S, poc:'hasHeight', literal(type(_, H))), atom_number(H, HeightL), roundD(HeightL, Height)) ; Height = 'unknown'), !.

get_dimensions_in_objects(S, [Depth, Width]):-  % get the dimensions of a shelf in the number of objects that fit on it in the depth and width directions
	rdf_has(S, poc:'hasInterRowDistance', literal(type(_, Da))), atom_number(Da, D),
	shelf_type(S, Type),
	get_dimensions(Type, [PD, PW| _]),  % get the dimensions of the type
	get_dimensions(S, [SD, SW| _]),  % get the dimensions of the shelf
	roundD(SD / PD, Depth),
	roundD(SW / (PW + D), Width), !.  % width of a row is object width plus inter-row distance

get_pose(S, [X, Y, Theta, Z]):-
	rdf_resource(S),  % S needs to be a resource
	rdfs_individual_of(S, poc:'Shelf'),
	rdf_has(S, poc:'hasPose', Pose),  % get the pose of object or shelf S
	((rdf_has(Pose, poc:'hasXCoordinate', literal(type(_, XLa))), atom_number(XLa, XL), roundD(XL, X)) ; X = 'unknown'),  % round to solve floating point inaccuracy
	((rdf_has(Pose, poc:'hasYCoordinate', literal(type(_, YLa))), atom_number(YLa, YL), roundD(YL, Y)) ; Y = 'unknown'),
	((rdf_has(Pose, poc:'hasZCoordinate', literal(type(_, ZLa))), atom_number(ZLa, ZL), roundD(ZL, Z)) ; Z = 'unknown'),
	((rdf_has(Pose, poc:'hasThetaCoordinate', literal(type(_, ThetaLa))), atom_number(ThetaLa, ThetaL), roundD(ThetaL, Theta)) ; Theta = 'unknown'), !.
get_pose(S, [X, Y, Theta, Z]):-
	rdf_resource(S),  % S needs to be a resource
	rdf_has(S, poc:'hasPose', Pose),  % get the pose of object or shelf S
	((rdf_has(Pose, poc:'hasXCoordinate', literal(type(_, XL))), roundD(XL, X)) ; X = 'unknown'),  % round to solve floating point inaccuracy
	((rdf_has(Pose, poc:'hasYCoordinate', literal(type(_, YL))), roundD(YL, Y)) ; Y = 'unknown'),
	((rdf_has(Pose, poc:'hasZCoordinate', literal(type(_, ZL))), roundD(ZL, Z)) ; Z = 'unknown'),
	((rdf_has(Pose, poc:'hasThetaCoordinate', literal(type(_, ThetaL))), roundD(ThetaL, Theta)) ; Theta = 'unknown'), !.

get_manipulability(Object, Back):-
	physical_object(Object),  % Object needs to be a resource
	rdf_has(Object, poc:'hasManipulability', Manip),  % get the manipulability of Object
	(rdf_has(Manip, poc:'hasBackManipulability', literal(type(_, Back))) ; Back = 'unknown'), !.

shelf_capacity(Shelf, Cap):-  % Shelf is supplied as resource, Cap is querried or checked
	\+var(Shelf),  % if Shelf is a variable, go on to the next definition
	rdf_resource(Shelf),
	rdfs_individual_of(Shelf, poc:'Shelf'),  % check if Shelf is actually a shelf
	(rdf_has(Shelf, poc:'hasCapacity', literal(type(_, Capa))), atom_number(Capa, Cap) ; Cap = 'unknown'), !.
shelf_capacity(Shelf, Cap):-  % Cap is supplied, Shelf is querried
	var(Shelf),  % Shelf must be a variable
	rdf_has(Shelf, poc:'hasCapacity', literal(type(_, Cap))),  % get a shelf that has this capacity
	rdf_resource(Shelf),  % check if the retrieved item is a resource
	rdfs_individual_of(Shelf, poc:'Shelf').  % check if the retrieved resource is a shelf

shelf_type(Shelf, Type):-
	var(Shelf) ->  % if Shelf is a variable
		(rdf_has(Type, poc:'typeContainedBy', Shelf), rdfs_individual_of(Shelf, poc:'Shelf')) ;  % get the shelf
	rdf_has(Shelf, poc:'containsType', Type),  % else, get the type
	rdfs_individual_of(Shelf, poc:'Shelf'), !.  % Shelf needs to be a shelf

active_shelf(Shelf):-  % request the shelf that is currently active
	rdf_has(Shelf, poc:'isCurrentlyWorked', true),
	rdf_resource(Shelf),  % check if the found item is a resource.
	rdfs_individual_of(Shelf, poc:'Shelf'), !.  % check if the found item is a shelf
active_shelf(_):-  % in case no active shelf was found
	print_message(warning, no_active_shelf),
	fail.

shelf_full:-  % true if the currently worked shelf is full
	active_shelf(Shelf),
	shelf_capacity(Shelf, Cap),
	list_objects(Obj),
	length(Obj, N),
	N >= Cap,  % True if the number of objects on the shelf equals or exceeds the capacity of the shelf
	!.

no_moves_left:-  % succeeds if the system has no moves left to perform
	active_shelf(_),  % a shelf needs to be active
	forall(row_pose(R, _),  % for all rows
		(front_occupied(R, Obj),  % the front is occupied
		 get_manipulability(Obj, false))).  % by an object that is blocked

shelf_done:-  % succeeds if the system is done with this shelf
	shelf_full.  % if the shelf is full, the system is done
shelf_done:-
	no_moves_left.  % if there are no more moves to perform

object_at(Object, [X, Y, T]):-
	rdf_has(Object, rdf:'type', poc:'Object'),  % get a object
	get_pose(Object, [X1, Y1, T1| _]),  % get its pose
	check_valid_pose([X1, Y1, T1]),
	((var(X)) -> (X is X1) ; true),  % if a coordinate is not given
	((var(Y)) -> (Y is Y1) ; true),  % copy it from what the knowledge base says
	((var(T)) -> (T is T1) ; true),
	match_pose([X1, Y1, T1], [X, Y, T]).  % compare the knowledge base pose with the supplied pose

match_pose([X1, Y1, T1], [X2, Y2, T2]):-
	check_valid_pose([X1, Y1, T1]),  % check if the poses are valid
	check_valid_pose([X2, Y2, T2]),
	active_shelf(S),
	shelf_type(S, Type),
	get_dimensions(Type, [Depth, Width| _]),
	pose_error_quotient(Q),  % get the error quotient
	roundD(abs(X1 - X2) * Q, DX),
	roundD(abs(Y1 - Y2) * Q, DY),
	roundD(abs(T1 - T2) * Q, DT),
	DX < Width,  % as long as all three coordinates are close enough
	DY < Depth,
	DT < 1, !.  % the poses can be considered the same

row_pose(R, X):-  % link a row number to an X coordinate (Row 0 is all the way on the left, numbers increasing to the right when facing the shelf)
	integer(R), var(X),  % if R is supplied and X is wanted
	active_shelf(S),
	shelf_type(S, T),
	get_dimensions(T, [_, PW| _]),
	get_dimensions_in_objects(S, [_, SW| _]),
	R >= 0,  % R needs to be part of the shelf
	R < SW,
	rdf_has(S, poc:'hasInterRowDistance', literal(type(_, Da))), atom_number(Da, D),
	roundD((R * (PW + D)) + ((PW + D) / 2), X), !.
row_pose(R, X):-  % link a row number to an X coordinate (Row 0 is all the way on the left, numbers increasing to the right when facing the shelf)
	number(X),  % if X is supplied and R is either supplied as well or wanted
	active_shelf(S),
	shelf_type(S, T),
	get_dimensions(T, [_, PW| _]),
	get_dimensions_in_objects(S, [_, SW| _]),
	rdf_has(S, poc:'hasInterRowDistance', literal(type(_, Da))), atom_number(Da, D),
	roundD((X - (PW + D) / 2) / (PW + D), R), 
	integer(R),
	R >= 0,  % R needs to be part of the shelf
	R < SW, !.
row_pose(R, X):-  % in case neither R nor X are supplied
	var(R), var(X),
	active_shelf(S),
	get_dimensions_in_objects(S, [_, NR| _]), !,  % get the number of rows
	MaxR is NR - 1,  % the maximum row number is the number of rows minus 1
	between(0, MaxR, R),  % get a row number
	row_pose(R, X).  % find the corresponding x coordinate

obj_inRow(Obj, R):-  % returns one object that is in row R on the shelf
    row_pose(R, X),  % get the X coordinate of the row
    rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
    object_at(Obj, [X| _]).  % match the X coordinate

objs_inRow(Objs, R):-  % return a list of objects in row R
    findall(X, obj_inRow(X, R), Objs).

nobjs_inRow(N, R):-  % returns the number of objects in row R
	objs_inRow(Objs, R),
	length(Objs, N).

front_occupied(R, Obj):-  % succeeds if the front pose of row R should be considered to be occupied by object Obj
	row_pose(R, X),
    rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
    object_at(Obj, [X, Y| _]),  % match the X coordinate and retrieve the Y coordinate
    get_dimensions(Obj, [PD| _]),
	pose_error_quotient(Q),  % get the error quotient
    roundD(PD * 3 / 2 - PD / Q, YUpperLimit),
    Y < YUpperLimit.  % Y needs to be smaller than any coordinate that leaves it at the second to most front pose

front_Y(Y):-  % returns the Y coordinate an object should have to be exactly in the front
	active_shelf(S),
	shelf_type(S, T),
	get_dimensions(T, [PD| _]),
	roundD(PD / 2, Y).  % half a object depth

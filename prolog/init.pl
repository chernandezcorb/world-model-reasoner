% Knowrob dependencies
%% :- register_ros_package(knowrob_common).  % Knowrob_common

% Import libraries
:- use_module(library('semweb/rdfs')).
:- use_module(library('semweb/rdf_db')).

% Import this packages ontology
:- rdf_load('../owl/PoC_ontology.owl').
:- rdf_register_prefix(poc, 'http://www.tudelft.org/arthur/proof-of-concept/ontology#', [keep(true)]).

% Import this packages prolog modules
:- use_module(library('poc_module')).
:- use_module(library('comp_spatial')).
:- use_module(library('ask_world_model')).
:- use_module(library('tell_world_model')).
:- use_module(library('reason_world_model')).
:- use_module(library('auto_update_world_model')).
:- use_module(library('observations')).
:- use_module(library('detections')).
:- use_module(library('actions')).
:- module(comp_spatial,
    [
      obj_toTheLeftOf/2,
      objs_toTheLeftOf/2,
      obj_toTheDirectLeftOf/2,
      objs_toTheDirectLeftOf/2,
      obj_toTheRightOf/2,
      objs_toTheRightOf/2,
      obj_toTheDirectRightOf/2,
      objs_toTheDirectRightOf/2,
      obj_inFrontOf/2,
      objs_inFrontOf/2,
      obj_inDirectFrontOf/2,
      objs_inDirectFrontOf/2,
      obj_behind/2,
      objs_behind/2,
      obj_directBehind/2,
      objs_directBehind/2,
      obj_objectAtPoint2D/2,
      objs_objectAtPoint2D/2,
      obj_intersectingObject/2,
      objs_intersectingObject/2,
      obj_atBack/1,
      objs_atBack/1,
      obj_atLeft/1,
      objs_atLeft/1,
      obj_atRight/1,
      objs_atRight/1,
      obj_inContact/2,
      check_valid_pose/1
    ]).
/** <module> Predicates for spatial reasoning

@author Arthur Helsloot
*/
:- use_module(library('poc_module')).
:- use_module(library('ask_world_model')).
:- use_module(library('reason_world_model')).

% define predicates as rdf_meta predicates
% (i.e. rdf namespaces are automatically expanded)
:-  rdf_meta
  obj_toTheLeftOf(r, r),
  objs_toTheLeftOf(+, r),
  obj_toTheDirectLeftOf(r, r),
  objs_toTheDirectLeftOf(+, r),
  obj_toTheRightOf(r, r),
  objs_toTheRightOf(+, r),
  obj_toTheDirectRightOf(r, r),
  objs_toTheDirectRightOf(+, r),
  obj_inFrontOf(r, r),
  objs_inFrontOf(+, r),
  obj_inDirectFrontOf(r, r),
  objs_inDirectFrontOf(+, r),
  obj_behind(r, r),
  objs_behind(+, r),
  obj_directBehind(r, r),
  objs_directBehind(+, r),
  obj_objectAtPoint2D(r, +),
  objs_objectAtPoint2D(+, +),
  obj_intersectingObject(r, r),
  objs_intersectingObject(+, r),
  obj_atBack(r),
  objs_atBack(+),
  obj_atLeft(r),
  objs_atLeft(+),
  obj_atRight(r),
  objs_atRight(+),
  obj_inContact(r, r).

obj_toTheLeftOf(Obj, Right):-  % returns a single objects Obj that is to the left of Right
  physical_object(Right),  % Right must be an object
  object_at(Right, [RX, RY, _]),  % get the pose
  number(RX), number(RY),  % the pose needs to be known
  get_dimensions(Right, [Depth, Width| _]),  % get the dimensions
  number(Depth), number(Width), !,  % do not backtrack to before this point, for efficiency

  rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
  Obj \= Right,  % Obj must not be the same as Right
  object_at(Obj, [OX, OY| _]),  % get the pose
  number(OX), number(OY),  % the values need to be known
  pose_error_quotient(Q),
  roundD(abs(OY - RY) - 2 * Depth / Q, DY),
  DY < Depth,  % Y difference needs to be strictly less than Depth
  OX < RX,              % X needs to be smaller for left than for right
  physical_object(Obj).  % Obj needs to be an object

objs_toTheLeftOf(Objects, Right):-  % returns list Objects with all objects to the left of Right
  findall(X,  % find all X
    obj_toTheLeftOf(X, Right),  % for which X is to the left of Right
  Objects).

obj_toTheDirectLeftOf(Obj, Right):-  % returns a single object Obj that is directly to the left of Right
  obj_toTheLeftOf(Obj, Right),
  obj_inContact(Obj, Right).

objs_toTheDirectLeftOf(Objs, Right):-  % returns a list of objects Objs that are directly to the left of Right
  findall(X, obj_toTheDirectLeftOf(X, Right), Objs).


obj_toTheRightOf(Obj, Left):-  % returns a single objects Obj that is to the right of Left
  physical_object(Left),  % Left must be an object
  object_at(Left, [LX, LY, _]),  % get the pose
  number(LX), number(LY),  % the pose needs to be known
  get_dimensions(Left, [Depth, Width| _]),  % get the dimensions
  number(Depth), number(Width), !,  % do not backtrack to before this point, for efficiency

  rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
  Obj \= Left,  % Obj must not be the same as Left
  object_at(Obj, [OX, OY| _]),  % get the pose
  number(OX), number(OY),  % the values need to be known
  pose_error_quotient(Q),
  roundD(abs(OY - LY) - 2 * Depth / Q, DY),
  DY < Depth,  % Y difference needs to be strictly less than Depth
  OX > LX,              % X needs to be smaller for left than for right
  physical_object(Obj).  % Obj needs to be an object

objs_toTheRightOf(Objects, Left):-  % returns list Objects with all objects to the right of Left
  findall(X,  % find all X
    obj_toTheLeftOf(X, Left),  % for which X is to the right of Left
  Objects).

obj_toTheDirectRightOf(Obj, Left):-  % returns a single object Obj that is directly to the right of Left
  obj_toTheRightOf(Obj, Left),
  obj_inContact(Obj, Left).

objs_toTheDirectRightOf(Objs, Left):-  % returns a list objects Objs that are directly to the right of Left
  findall(X, obj_toTheDirectRightOf(X, Left), Objs).
  

obj_inFrontOf(Obj, Back):-  % returns a single objects Obj that is in front of Back
  physical_object(Back),  % Back must be an object
  object_at(Back, [BX, BY, _]),  % get the pose
  number(BX), number(BY),  % the pose needs to be known
  get_dimensions(Back, [Depth, Width| _]),  % get the dimensions
  number(Depth), number(Width), !,  % do not backtrack to before this point, for efficiency

  rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
  Obj \= Back,  % Obj must not be the same as Back
  object_at(Obj, [OX, OY| _]),  % get the pose
  number(OX), number(OY),  % the values need to be known
  pose_error_quotient(Q),
  roundD(abs(OX - BX) - 2 * Width / Q, DX),
  DX < Width,  % X difference needs to be strictly less than Width
  OY < BY,              % Y needs to be smaller for front than for Back
  physical_object(Obj).  % Obj needs to be an object

objs_inFrontOf(Objects, Back):-  % returns list Objects with all objects in front of Back
  findall(X,  % find all X
    obj_inFrontOf(X, Back),  % for which X is in front of Back
  Objects).

obj_inDirectFrontOf(Obj, Back):-  % returns a single object Obj that is directly in front of Back
  obj_inFrontOf(Obj, Back),
  obj_inContact(Obj, Back).

objs_inDirectFrontOf(Objs, Back):-  % returns a list objects Objs that are directly in front of Back
  findall(X, obj_inDirectFrontOf(X, Back), Objs).


obj_behind(Obj, Front):-  % returns a single objects Obj that is behind Front
  physical_object(Front),  % Front must be an object
  object_at(Front, [FX, FY, _]),  % get the pose
  number(FX), number(FY),  % the pose needs to be known
  get_dimensions(Front, [Depth, Width| _]),  % get the dimensions
  number(Depth), number(Width), !,  % do not backtrack to before this point, for efficiency

  rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
  Obj \= Front,  % Obj must not be the same as Front
  object_at(Obj, [OX, OY| _]),  % get the pose
  number(OX), number(OY),  % the values need to be known
  pose_error_quotient(Q),
  roundD(abs(OX - FX) - 2 * Width / Q, DX),
  DX < Width,  % X difference needs to be strictly less than Width
  OY > FY,              % Y needs to be smaller for Front than for back
  physical_object(Obj).  % Obj needs to be an object

objs_behind(Objects, Front):-  % returns list Objects with all objects behind Front
  findall(X,  % find all X
    obj_behind(X, Front),  % for which X is behind Front
  Objects).

obj_directBehind(Obj, Front):-  % returns a single object Obj that is directly behind Front
  obj_behind(Obj, Front),
  obj_inContact(Obj, Front).

objs_directBehind(Objs, Front):-  % returns a list objects Objs that are directly behind Front
  findall(X, obj_directBehind(X, Front), Objs).


obj_objectAtPoint2D(Obj, [PX, PY]):-  % returns an object Obj that intersects with Point = [PX, PY]
  rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
  get_dimensions(Obj, [OD, OW| _]),  % get the object's dimensions
  number(OD), number(OW),  % the values need to be known
  object_at(Obj, [OX, OY| _]),  % get the pose
  number(OX), number(OY),  % the values need to be known
  pose_error_quotient(Q),
  roundD(OX + (OW / 2) + OW / Q, XUpperlimit),  % calculate the limits and round them
  roundD(OX - (OW / 2) - OW / Q, XLowerlimit),
  roundD(OY + (OD / 2) + OD / Q, YUpperlimit),
  roundD(OY - (OD / 2) - OD / Q, YLowerlimit),
  roundD(PX, PXR),  % round all poses to solve floating point inaccuracy
  roundD(PY, PYR),
  PXR < XUpperlimit,  % check if its values fall in the correct range
  PXR > XLowerlimit,
  PYR < YUpperlimit,
  PYR > YLowerlimit,
  physical_object(Obj).  % Obj needs to be an object

objs_objectAtPoint2D(Objs, Point):-  % returns a list of objects Objs that intersect with Point = [PX, PY]
  findall(X,  % find all X
    obj_objectAtPoint2D(X, Point),  % for which X intersects Point
  Objs).

obj_atBack(Obj):-  % returns one object that is at the back of the shelf
  active_shelf(Shelf),  % get the currently worked shelf
  get_dimensions(Shelf, [SD| _]),  % get the shelf's depth
  shelf_type(Shelf, Type),
  get_dimensions(Type, [OD| _]),  % get the depth of the objects on the shelf
  number(SD), number(OD), !,  % do not backtrack to before this point, for efficiency

  rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
  object_at(Obj, [_, OY| _]),  % get the pose
  number(OY),  % the values need to be known
  pose_error_quotient(Q), 
  roundD(OY + (OD / 2) + (OD / Q), BackY),
  BackY >= SD.

objs_atBack(Objs):-  % return a list of objects at the back of the shelf
  findall(X, obj_atBack(X), Objs).

obj_atLeft(Obj):-  % returns one object that is at the left side of the shelf
  active_shelf(Shelf),  % get the currently worked shelf
  shelf_type(Shelf, Type),
  get_dimensions(Type, [_, OW| _]),  % get the width of the objects on the shelf
  number(OW),
  !,  % do not backtrack to before this point, for efficiency

  rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
  object_at(Obj, [OX| _]),  % get the pose
  number(OX),  % the values need to be known
  pose_error_quotient(Q),
  roundD(OX - (OW / 2) - (OW / Q), SideX),
  SideX =< 0,  % X needs to be at the left
  physical_object(Obj).  % Obj needs to be an object

objs_atLeft(Objs):-  % returns a list of objects at the left side of the shelf
  findall(X, obj_atLeft(X), Objs).

obj_atRight(Obj):-  % returns one object that is at the right side of the shelf
  active_shelf(Shelf),  % get the currently worked shelf
  get_dimensions(Shelf, [_, SW| _]),  % get the shelf's width
  shelf_type(Shelf, Type),
  get_dimensions(Type, [_, OW| _]),  % get the width of the objects on the shelf
  number(SW), number(OW),
  !,  % do not backtrack to before this point, for efficiency

  rdf_has(Obj, rdf:'type', poc:'Object'),  % find an object element
  object_at(Obj, [OX| _]),  % get the pose
  number(OX),  % the values need to be known
  pose_error_quotient(Q),
  roundD(OX + (OW / 2) + (OW / Q), SideX),
  SideX >= SW,  % X needs to be at the right
  physical_object(Obj).  % Obj needs to be an object

objs_atRight(Objs):-  % returns a list of objects at the right side of the shelf
  findall(X, obj_atRight(X), Objs).

obj_intersectingObject(Answer, Object):-  % returns an object Answer that intersect the area occupied by Object
  physical_object(Object),  % Object needs to be an object
  object_at(Object, [PX, PY| _]),
  get_dimensions(Object, [PD, PW| _]), 
  number(PX), number(PY), number(PW), number(PD), !,  % do not backtrack above this point, for efficiency

  rdf_has(Answer, rdf:'type', poc:'Object'),  % find an object element
  Answer \= Object,  % Answer must not be Object
  object_at(Answer, [OX, OY| _]),  % get the pose
  number(OX), number(OY),  % the values need to be known
  pose_error_quotient(Q),
  roundD(PX + PW - 2 * PW / Q, XUpperlimit),  % calculate the limits and round them 
  roundD(PX - PW + 2 * PW / Q, XLowerlimit),
  roundD(PY + PD - 2 * PD / Q, YUpperlimit),
  roundD(PY - PD + 2 * PD / Q, YLowerlimit),
  roundD(OX, XR),  % round all poses to solve floating point inaccuracy
  roundD(OY, YR),
  XR < XUpperlimit,  % if must be intersecting
  XR > XLowerlimit,
  YR < YUpperlimit,
  YR > YLowerlimit,
  physical_object(Answer).  % Answer needs to be an object

objs_intersectingObject(Answers, Object):-  % returns a list of objects Answers that intersect the area occupied by Object
  findall(X,  % find all X
    obj_intersectingObject(X, Object),  % for which X intersects Object
  Answers).

obj_inContact(Answer, Object):-  % returns a single object Answer that is in contact with Object
  physical_object(Object),  % Object needs to be an object
  object_at(Object, [PX, PY| _]),
  get_dimensions(Object, [PD, PW| _]), 
  number(PX), number(PY), number(PW), number(PD), !,  % do not backtrack above this point, for efficiency

  rdf_has(Answer, rdf:'type', poc:'Object'),  % find an object element
  Answer \= Object,  % Answer must not be Object
  object_at(Answer, [OX, OY| _]),  % get the pose
  number(OX), number(OY),  % the values need to be known
  pose_error_quotient(Q),
  roundD(PX + PW + 2 * PW / Q, XUpperlimit),  % calculate the limits and round them
  roundD(PX - PW - 2 * PW / Q, XLowerlimit),
  roundD(PY + PD + 2 * PD / Q, YUpperlimit),
  roundD(PY - PD - 2 * PD / Q, YLowerlimit),
  roundD(OX, XR),  % round all poses to solve floating point inaccuracy
  roundD(OY, YR),
  XR =< XUpperlimit,
  XR >= XLowerlimit,
  YR =< YUpperlimit,
  YR >= YLowerlimit,
  \+obj_intersectingObject(Answer, Object).  % Answer must not be intersecting Object

check_valid_pose([X, Y, Theta]):-  % succeeds if the input is a valid pose
  ((number(X), number(Y), number(Theta)) -> true ; (print_message(warning, pose_not_a_number(X, Y, Theta)), !, fail) ),  % the pose needs to consist of numbers
  %% active_shelf(Shelf),
  %% get_dimensions(Shelf, [SD, SW| _]),
  %% shelf_type(Shelf, Type),
  %% get_dimensions(Type, [PD, PW| _]),
  %% pose_error_quotient(Q), !,
  %% roundD(SW - (PW / 2) + PW / Q, XUpperlimit),  % calculate the limits and round them
  %% roundD(PW / 2 - PW / Q, XLowerlimit),
  %% roundD(SD - (PD / 2) + PW / Q, YUpperlimit),
  %% roundD(PD / 2 - PW / Q, YLowerlimit),
  %% roundD(X, XR),  % round all poses to solve floating point inaccuracy
  %% roundD(Y, YR),
  %% roundD(Theta, ThetaR),
  %% (XR =< XUpperlimit,  % the pose needs to fit on the shelf
  %%  XR >= XLowerlimit,
  %%  YR =< YUpperlimit,
  %%  YR >= YLowerlimit) ; (print_message(warning, pose_outside_shelf([X, Y, Theta])), fail),
  !.

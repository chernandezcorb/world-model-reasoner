:- module(proof_of_concept,[
	fofoa/1,
	float_decimal_accuracy/1,
	pose_error_quotient/1,
	roundD/2,
	roundD/3,
	time_before/2,
	time_after/2,
	rdf_instance_from_class/2,
	save_ontology/0,
	load_ontology/0
	]).

:- rdf_meta
	rdf_instance_from_class(r, t).

%%%%%%%%%%%%%%%%% Useful things %%%%%%%%%%%%%%%%%

fofoa(1000).  % 1/X is fraction of fitting objects assumed (1->all, 2->half, 9999999->none)
float_decimal_accuracy(6).  % the accuracy in number of decimals the system should deal with
pose_error_quotient(3).  % poses may differ by D/Q for the object to still be considered at that pose, where D is the objects dimension and Q is the quotient defined here

roundD(X, Y):-  % if no number of decimals is specified, use the default defined above
	float_decimal_accuracy(D),
	roundD(X, Y, D).

roundD('unknown', 'unknown', _):- !.  % to not crash when a value is unknown
roundD(X, Y, D) :- Z is X * 10^D, round(Z, ZA), Y is ZA / 10^D.  % to round to a certain number of decimals


time_before([BSecs| _], [ASecs| _]):- BSecs < ASecs, !.  % Check if time Before is before time After
time_before([BSecs, BNSecs], [ASecs, ANSecs]):-
	BSecs == ASecs,
	BNSecs < ANSecs.

time_after(After, Before):- time_before(Before, After).  % Check if time After is after time Before

rdf_instance_from_class(Class, Instance) :-
  rdf_unique_id(Class, Instance),
  rdf_assert(Instance, rdf:type, Class).

rdf_unique_id(Class, UniqID) :-  % Copied from pmk.pl
  % generate 8 random alphabetic characters
  randseq(8, 25, Seq_random),
  maplist(plus(65), Seq_random, Alpha_random),
  atom_codes(Sub, Alpha_random),
  atom_concat(Class,  '_', Class2),
  atom_concat(Class2, Sub, Instance),
  % check if there is no triple with this identifier as subject or object yet
  ((rdf(Instance,_,_);rdf(_,_,Instance)) ->
    (rdf_unique_id(Class, UniqID));
    (UniqID = Instance)).

%%%%%%%%%%%%%%%%% For debugging %%%%%%%%%%%%%%%%%

save_ontology:-
	findall(P, rdf_current_prefix(P,_), NameSList),
	rdf_save('/home/arthur/knowrob_ws/src/world_model_reasoner/owl/saved_ontology.owl', [namespaces(NameSList)]).

load_ontology:-
	rdf_load('package://world_model_reasoner/owl/saved_ontology.owl').

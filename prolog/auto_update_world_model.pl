:- module(auto_update_world_model,[
	auto_update/1,
	auto_update_manipulability/0,
	fill_gap/3,
	assume_behind/2
	]).

:- use_module(library('poc_module')).
:- use_module(library('comp_spatial')).
:- use_module(library('ask_world_model')).
:- use_module(library('tell_world_model')).
:- use_module(library('reason_world_model')).
:- use_module(library('detections')).

%%%%%%%%%%%%%%%%% Auto-update ontology things %%%%%%%%%%%%%%%%%
auto_update(Time):-  % clears intersecting objects, propagates manipulability and infers object presence
	forall(rdf_has(Object, rdf:type, poc:'Object'), clear_intersecting_objects(Object)),  % there should be no intersecting objects in the ontology
	fix_gaps(Time).

fix_gaps(Time):-  % reason about presence of unseen objects and manipulability until you cannot conclude anything else
	auto_update_manipulability,  % first infer manipulability
	reason_gap(GPose, GDims), !,  % find a gap
	fill_gap(GPose, GDims, Time),  % fill the gap
	fix_gaps(Time).
fix_gaps(_):- !.  % if you cannot find gaps anymore, you are done

auto_update_manipulability():-  % reason about back manipulability for all objects and update the ontology accordingly
	list_objects(AllObjects),  % get all objects on the shelf
	findall(X, (
		member(X, AllObjects),  % find all objects X
		reason_backBlocked(X),  % that you can reason for them that they are backblocked
		update_manipulability(X, false)),  % then register them as backblocked
	_), !.

fill_gap(GapPose, [GDepth, GWidth], Time):-  % given a gap at GapPose = [GX, GY] with dimensions GDepth and GWidth, asserts infered objects to fill the gap, or fails
	active_shelf(Shelf),
	shelf_type(Shelf, Type),
	get_dimensions(Type, [ODepth, GWidth| _]),  % gap must be the width of an object
	roundD(GDepth / ODepth, N),  % round to solve floating point inaccuracy
	M is floor(N),  % get the integer that N should be
	roundD(GDepth - M * ODepth, Modulo),
	pose_error_quotient(Q),
	(Modulo < M * (ODepth / Q) ;  % modulo is small enough, within the error margins (GDepth = N * PDepth where N is an integer)
		(Modulo > ODepth - (ODepth / Q), M is M + 1)),  % or modulo is big enough to fit another object within error margins
	add_objects_to_zone(GapPose, M, Time).

add_objects_to_zone(_, N, _):-
	N =:= 0, !.  % the gap is filled
add_objects_to_zone([GX, GY], N, Time):-
	new_object(Object),  % make a new object
	update_pose(Object, [GX, GY, 0]),  % give it the pose of the gap
	add_detection(Object, poc:'inference', Detection),  % add a inference detection
	update_detection_time(Detection, Time),  % add time to the detection
	get_dimensions(Object, [ODepth| _]),
	GYnew is GY + ODepth,
	roundD(N - 1, Nnew),  % round to solve floating point inaccuracy
	add_objects_to_zone([GX, GYnew], Nnew, Time).  % add another object to the gap

assume_behind(Object, _):-  % assume objects behind Object. Used at first observation
	\+obj_inRow(Object, _).  % if the object is not in a row, you're done
assume_behind(Object, Time):-
	active_shelf(S),
	get_dimensions(S, [SD| _]),
	get_dimensions(Object, [OD| _]),
	object_at(Object, [X, Y| _]),
	fofoa(F),
	roundD(Y + OD, ZY),  % get the Y coordinate of the zone to fill
	M is floor((SD - Y - (OD / 2)) / OD),  % get the maximum number of objects that fit behind Object
	N is floor(M / F),  % decide the number of objects that are going to be infered
	add_objects_to_zone([X, ZY], N, Time).


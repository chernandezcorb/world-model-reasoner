:- module(tell_world_model,[
	start_shelf/1,
	stop_shelf/1,
	new_object/1,
	update_pose/2,
	update_manipulability/2,
	retract_object/1
	]).

:- use_module(library('poc_module')).
:- use_module(library('comp_spatial')).
:- use_module(library('ask_world_model')).
:- use_module(library('detections')).

:- rdf_meta
	start_shelf(r),
	stop_shelf(r),
	update_pose(r, +),
	update_manipulability(r, +),
	retract_object(r).

%%%%%%%%%%%%%%%%% Tell things %%%%%%%%%%%%%%%%%

start_shelf(Shelf):-  % start working on this Shelf
	rdfs_individual_of(Shelf, poc:'Shelf'),  % Shelf must be a shelf
	list_shelves(Shelves),
	maplist(stop_shelf, Shelves),  % stop working on all shelves
	rdf_retractall(Shelf, poc:'isCurrentlyWorked', _),  % remove triple saying Shelf is not worked on
	rdf_assert(Shelf, poc:'isCurrentlyWorked', 'true').  % add triple saying Shelf is worked on

stop_shelf(Shelf):-  % stop working on this Shelf
	rdfs_individual_of(Shelf, poc:'Shelf'),  % Shelf must be a shelf
	rdf_retractall(Shelf, poc:'isCurrentlyWorked', _),  % remove triple saying if Shelf is worked on
	rdf_assert(Shelf, poc:'isCurrentlyWorked', 'false'),  % add triple saying Shelf is not worked on
	forall(rdf_has(Object, rdf:'type', poc:'Object'),  % for every object
		retract_object(Object)).  % retract all knowledge of the object

new_object(Object):-  % create a new instance of Object without specified pose or manipulability
	(var(Object) -> true ; (print_message(warning, not_a_variable(Object)))),  % Object needs to be a variable
	active_shelf(Shelf),  % get the currently worked shelf
	rdf_instance_from_class(poc:'Object', Object),  % Create an instance of class Object
	rdf_instance_from_class(poc:'Manipulability', Manip),  % Create an instance of class Manipulability
	rdf_instance_from_class(poc:'Pose', Pose),  % Create an instance of class Pose
	rdf_assert(Object, poc:'hasPose', Pose),  % Link the Object to the Pose
	rdf_assert(Object, poc:'hasManipulability', Manip),  % Link the Object to the Manipulability
	rdf_assert(Object, poc:'onShelf', Shelf),  % Link the Object to the instance of the shelf it is on.
	shelf_type(Shelf, Type),
	rdf_assert(Object, poc:'hasType', Type),  % Link the Object to the type of object it belongs to
	rdf_assert(Object, poc:'hasID', literal(type(xsd:string, Object))), !.  % Give the new object an ID

update_pose(Object, [X, Y, Theta]):-  % update the pose of Object to the supplied values
	physical_object(Object),  % Object needs to be an object
	roundD(X, XR),  % round all poses to solve floating point inaccuracy
	roundD(Y, YR),
	roundD(Theta, ThetaR),
	check_valid_pose([XR, YR, ThetaR]),
	rdf_has(Object, poc:'hasPose', Pose),  % get the pose instance
	rdf_retractall(Pose, poc:'hasXCoordinate', _),  % delete the pose data if it had those
	rdf_retractall(Pose, poc:'hasYCoordinate', _),
	rdf_retractall(Pose, poc:'hasThetaCoordinate', _),
	rdf_assert(Pose, poc:'hasXCoordinate', literal(type(xsd:double, XR))),  % assert the new pose data to the ontology
	rdf_assert(Pose, poc:'hasYCoordinate', literal(type(xsd:double, YR))),
	rdf_assert(Pose, poc:'hasThetaCoordinate', literal(type(xsd:double, ThetaR))), !.

update_manipulability(Object, Back):-  % update the back manipulability of Object
	physical_object(Object),  % Object needs to be an object
	(atom(Back) -> true ; (print_message(warning, invalid_manipulability(Back)), fail) ),  % the manipulability needs to consist of atoms
	rdf_has(Object, poc:'hasManipulability', Manip),  % get the manipulability instance
	rdf_retractall(Manip, poc:'hasBackManipulability', _),  % delete the manipulability data if it had it
	rdf_assert(Manip, poc:'hasBackManipulability', literal(type(xsd:boolean, Back))), !.  % assert the new manipulability data to the ontology

retract_object(Object):-  % retract an object and everything associated to it from the ontology
	physical_object(Object),  % Object needs to be an object
	get_detections(Object, Detections),  % get all the object's detections
	rdf_has(Object, poc:'hasManipulability', Manip),  % get its manipulability instance
	rdf_has(Object, poc:'hasPose', Pose),  % get its pose instance
	forall(member(Det, Detections), retract_detection(Det)),  % retract all detections
	rdf_retractall(Manip, _, _),  % retract everything related to the manipulability
	rdf_retractall(_, _, Manip),
	rdf_retractall(Pose, _, _),  % retract everything related to the pose
	rdf_retractall(_, _, Pose),
	rdf_retractall(Object, _, _),  % retract everything left related to the object
	rdf_retractall(_, _, Object).
:- module(reason_world_model,[
	reason_manipulability/2,
	reason_backBlocked/1,
	reason_gap/2
	]).

:- use_module(library('poc_module')).
:- use_module(library('comp_spatial')).
:- use_module(library('ask_world_model')).

:- rdf_meta
	reason_manipulability(r, +),
	reason_backBlocked(r),
	reason_backBlocked1(r),
	reason_backBlocked2(r).

%%%%%%%%%%%%%%%%% Reason things %%%%%%%%%%%%%%%%%

reason_manipulability(Object, Back):-  % get the manipulability data of Object from the ontology or through reasoning
	(reason_backBlocked(Object) -> Back = false ; get_manipulability(Object, Back)), !.

reason_backBlocked(Object):-  % Object is back-blocked if the chain of objects behind it are back-blocked, or if the chain of objects in front of it are back-blocked
	(reason_backBlocked1(Object), !) ; reason_backBlocked2(Object).

reason_backBlocked1(Object):-  % Object is back-blocked if
	get_manipulability(Object, false), !.  % it is regirstered as back-blocked
reason_backBlocked1(Object):-  % Object is back-blocked if
	obj_atBack(Object), !.  % it is at the back
reason_backBlocked1(Object):-  % Object is back-blocked if an object behind it is back-blocked
	obj_directBehind(O, Object),  % get a object directly behind Object
	reason_backBlocked1(O), !.  % check if it is back-blocked

reason_backBlocked2(Object):-  % Object is back-blocked if
	get_manipulability(Object, false), !.  % it is regirstered as back-blocked
reason_backBlocked2(Object):-  % Object is back-blocked if there is only one object in front of it and this object is back-blocked
	objs_inDirectFrontOf([O], Object),  % get the object directly in front of Object
	reason_backBlocked2(O), !.  % check if it is back-blocked


reason_gap([GX, GY], [GDepth, GWidth]):-  % succeeds if there is a gap between (an object connected to) the back and a back-blocked object
	rdf_has(O, rdf:'type', poc:'Object'),  % There is an object O
	physical_object(O),  % O must be an object
	\+obj_directBehind(_, O),  % There is no object directly behind O
	\+obj_atBack(O),  % O is not at the back of the shelf
	reason_manipulability(O, false), !,  % but O is registered as back-blocked
	get_dimensions(O, [PDepth, GWidth| _]),  % Gap width is the same as object width
	object_at(O, [GX, OY| _]),  % Gap X coordinate is the same as for O
	roundD(OY + PDepth, GY),  % Gap Y coordinate is one object depth behind O
	get_gap_depth(GDepth, GX, GY, O, PDepth),
	\+ 0 =:= GDepth.  % the gap depth should be more than 0. This should be redundant due to \+obj_atBack(O), but due to floating point inaccuracy it is not

get_gap_depth(GDepth, _, GY, O, ODepth):-  % get the depth of the gap at GY, GX, where O is the object directly in front of the gap
	\+obj_behind(_, O),  % there are no objects behind O
	active_shelf(Shelf),
	get_dimensions(Shelf, [SDepth| _]),
	roundD(SDepth - GY + (ODepth / 2), GDepth).  % GDepth is until the back of the shelf, round to solve floating point inaccuracy
get_gap_depth(GDepth, GX, GY, O):-
	objs_behind(BackObjs, O),  % There are objects behind O
	member(Bob, BackObjs),
	objs_inFrontOf(FrontObjs, Bob),  % get a list FrontObjs of objects in front of Bob
	intersection(FrontObjs, BackObjs, []),  % there must be no overlap between FrontObjs and BackObjs, then Bob is the front most object behind the gap
	object_at(Bob, [BobX, BobY| _]),
	BobX =:= GX,  % Bob must have the same X coordinate as the gap
	roundD(BobY - GY, GDepth).  % Round to solve floating point inaccuracy
